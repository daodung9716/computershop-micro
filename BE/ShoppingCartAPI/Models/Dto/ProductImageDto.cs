﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ShoppingCartAPI.Models.Dto
{
    public class ProductImageDto
    {
        public Guid? Id { get; set; }
  
        public string? ImageUrl { get; set; }
        public Guid? ProductId { get; set; }
            
    }
}
