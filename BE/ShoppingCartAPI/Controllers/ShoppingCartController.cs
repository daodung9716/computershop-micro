﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingCartAPI.Data;
using ShoppingCartAPI.IService;
using ShoppingCartAPI.Models;
using ShoppingCartAPI.Models.Dto;
using ShoppingCartAPI.Service.IService;
using System.Security.Claims;

namespace ShoppingCartAPI.Controllers
{
    [ApiController]
    [Route("api/cart")]
    public class ShoppingCartController : ControllerBase
    {

        private ICartService _cartService;
        private IProductService _productService;
        private ICouponService _couponService;

        public ShoppingCartController(IProductService productService, ICouponService couponService, ICartService cartService)
        {

            _productService = productService;
            _couponService = couponService;
            _cartService = cartService;
        }

        [HttpGet("GetCart")]
        [Authorize]
        public async Task<IActionResult> GetCart()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            ResponseDto res = await _cartService.GetCart(userId);
            
            return Ok(res);

        }

        [HttpGet("GetCartCheckout")]
        [Authorize]
        public async Task<IActionResult> GetCartCheckout()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            ResponseDto res = await _cartService.GetCartCheckout(userId);
            
            return Ok(res);
        }


        [HttpPost("ApplyCoupon")]
        [Authorize]
        public async Task<IActionResult> ApplyCoupon([FromBody] CartDto cartDto)
        {
            ResponseDto res = await _cartService.ApplyCoupon(cartDto);
            
            return Ok(res);
        }




        [HttpPost("CartUpsert")]
        [Authorize]
        public async Task<IActionResult> CartUpsert(CartDto cartDto)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            cartDto.CartHeader.UserId = userId;
            ResponseDto res = await _cartService.CartUpsert(cartDto, userId);
            
            return Ok(res);
        }



        [HttpPost("CartUpsertBulk")]
        [Authorize]
        public async Task<IActionResult> CartUpsertBulk(CartDto cartDto)
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            cartDto.CartHeader.UserId = userId;
            ResponseDto res = await _cartService.CartUpsertBulk(cartDto, userId);
           
            return Ok(res);

        }



        [Authorize]
        [HttpPost("RemoveCart")]
        public async Task<IActionResult> RemoveCart()
        {
            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            ResponseDto res = await _cartService.RemoveCart( userId);
           
            return Ok(res);
        }

        [Authorize]
        [HttpPost("RemoveCartDetail")]
        public async Task<IActionResult> RemoveCartDetail(int cardHeaderId, int cartDetailId)
        {
                      
            ResponseDto res = await _cartService.RemoveCartDetail(cardHeaderId,cartDetailId);
           
            return Ok(res);
        }

    }

}