﻿

using ShoppingCartAPI.Models.Dto;

namespace ShoppingCartAPI.IService
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> GetProducts();
    }
}
