﻿

using ShoppingCartAPI.Models.Dto;

namespace ShoppingCartAPI.IService
{
    public interface ICouponService
    {
        Task<CouponDto> GetCoupon(string couponCode);
    }
}
