﻿using Microsoft.AspNetCore.Mvc;
using ShoppingCartAPI.Models.Dto;
using System.Threading.Tasks;

namespace ShoppingCartAPI.Service.IService
{
    public interface ICartService
    {

        Task<ResponseDto> GetCart(string userId);

        Task<ResponseDto> GetCartCheckout(string userId);

        Task<ResponseDto> ApplyCoupon( CartDto cartDto);

        Task<ResponseDto> CartUpsert(CartDto cartDto,string userId);

        Task<ResponseDto> CartUpsertBulk(CartDto cartDto, string userId);

        Task<ResponseDto> RemoveCartDetail(int cardHeaderId, int cartDetailId);
        Task<ResponseDto> RemoveCart(string userId);
    }
}
