﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ShoppingCartAPI.Data;
using ShoppingCartAPI.IService;
using ShoppingCartAPI.Models;
using ShoppingCartAPI.Models.Dto;
using ShoppingCartAPI.Service.IService;

namespace ShoppingCartAPI.Service
{
    public class CartService : ICartService
    {
        private ResponseDto _response;
        private IMapper _mapper;
        private readonly AppDbContext _db;
        private IProductService _productService;

        public CartService(AppDbContext db, IMapper mapper, IProductService productService)
        {
            _db = db;
            _response = new ResponseDto();
            _mapper = mapper;
            _productService = productService;
        }

        public async Task<ResponseDto> GetCart(string userId)
        {
            try
            {
                CartDto cart = new()
                {
                    CartHeader = _mapper.Map<CartHeaderDto>(_db.CartHeaders.First(u => u.UserId == userId))
                };

                cart.CartDetails = _mapper.Map<IEnumerable<CartDetailsDto>>(_db.CartDetails
                    .Where(u => u.CartHeaderId == cart.CartHeader.CartHeaderId));

                IEnumerable<ProductDto> productDtos = await _productService.GetProducts();

                foreach (var item in cart.CartDetails)
                {
                    item.Product = productDtos.FirstOrDefault(u => u.Id == item.ProductId);
                    cart.CartHeader.CartTotal += (item.Count * item.Product.Price);
                }

                _response.Result = cart;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        public async Task<ResponseDto> GetCartCheckout(string userId)
        {
            try
            {
                var user = _db.AspNetUsers.Where(x => x.Id == userId).FirstOrDefault();
                CartDto cart = new()
                {
                    CartHeader = _mapper.Map<CartHeaderDto>(_db.CartHeaders.First(u => u.UserId == userId))
                };
                cart.CartHeader.Name = user.Name;
                cart.CartHeader.Phone = user.PhoneNumber;
                cart.CartHeader.Address = user.Address;
                cart.CartHeader.Email = user.Email;


                cart.CartDetails = _mapper.Map<IEnumerable<CartDetailsDto>>(_db.CartDetails
                    .Where(u => u.CartHeaderId == cart.CartHeader.CartHeaderId));

                IEnumerable<ProductDto> productDtos = await _productService.GetProducts();

                foreach (var item in cart.CartDetails)
                {
                    item.Product = productDtos.FirstOrDefault(u => u.Id == item.ProductId);
                    cart.CartHeader.CartTotal += (item.Count * item.Product.Price);
                }

                _response.Result = cart;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        public async Task<ResponseDto> ApplyCoupon(CartDto cartDto)
        {
            try
            {
                var cartFromDb = await _db.CartHeaders.FirstAsync(u => u.UserId == cartDto.CartHeader.UserId);
                cartFromDb.CouponCode = cartDto.CartHeader.CouponCode;
                _db.CartHeaders.Update(cartFromDb);
                await _db.SaveChangesAsync();
                _response.Result = true;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.ToString();
            }
            return _response;
        }

        public async Task<ResponseDto> CartUpsert(CartDto cartDto, string userId)
        {
            try
            {
                var cartHeaderFromDb = await _db.CartHeaders.AsNoTracking().FirstOrDefaultAsync(u => u.UserId == userId);

                if (cartHeaderFromDb == null)
                {
                    CartHeader cartHeader = _mapper.Map<CartHeader>(cartDto.CartHeader);
                    _db.CartHeaders.Add(cartHeader);
                    await _db.SaveChangesAsync();

                    foreach (var cartDetailDto in cartDto.CartDetails)
                    {
                        cartDetailDto.CartHeaderId = cartHeader.CartHeaderId;
                        _db.CartDetails.Add(_mapper.Map<CartDetails>(cartDetailDto));
                    }               
                    await _db.SaveChangesAsync();
                }
                else
                {
                    foreach (var cartDetailDto in cartDto.CartDetails)
                    {
                        var cartDetailsFromDb = await _db.CartDetails.AsNoTracking().FirstOrDefaultAsync(
                            u => u.ProductId == cartDetailDto.ProductId && u.CartHeaderId == cartHeaderFromDb.CartHeaderId);

                        if (cartDetailsFromDb == null)
                        {
                            cartDetailDto.CartHeaderId = cartHeaderFromDb.CartHeaderId;
                            _db.CartDetails.Add(_mapper.Map<CartDetails>(cartDetailDto));
                        }
                        else
                        {
                            cartDetailDto.Count += cartDetailsFromDb.Count;
                            cartDetailDto.CartHeaderId = cartDetailsFromDb.CartHeaderId;
                            cartDetailDto.CartDetailsId = cartDetailsFromDb.CartDetailsId;
                            _db.CartDetails.Update(_mapper.Map<CartDetails>(cartDetailDto));
                        }
                    }
                    _response.Message = "Add Cart Successfully";
                    await _db.SaveChangesAsync();
                }
                _response.Result = cartDto;
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message.ToString();
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDto> CartUpsertBulk(CartDto cartDto, string userId)
        {
            try
            {
                var cartHeaderFromDb = await _db.CartHeaders.AsNoTracking().FirstOrDefaultAsync(u => u.UserId == userId);

                if (cartHeaderFromDb == null)
                {
                    CartHeader cartHeader = _mapper.Map<CartHeader>(cartDto.CartHeader);
                    _db.CartHeaders.Add(cartHeader);
                    await _db.SaveChangesAsync();

                    foreach (var cartDetailDto in cartDto.CartDetails)
                    {
                        cartDetailDto.CartHeaderId = cartHeader.CartHeaderId;
                        _db.CartDetails.Add(_mapper.Map<CartDetails>(cartDetailDto));
                    }
                    _response.Message = "Add Cart Successfully";
                    await _db.SaveChangesAsync();
                }
                else
                {
                    foreach (var cartDetailDto in cartDto.CartDetails)
                    {
                        var cartDetailsFromDb = await _db.CartDetails.AsNoTracking().FirstOrDefaultAsync(
                            u => u.ProductId == cartDetailDto.ProductId && u.CartHeaderId == cartHeaderFromDb.CartHeaderId);

                        if (cartDetailsFromDb == null)
                        {
                            cartDetailDto.CartHeaderId = cartHeaderFromDb.CartHeaderId;
                            _db.CartDetails.Add(_mapper.Map<CartDetails>(cartDetailDto));
                        }
                        else
                        {
                            cartDetailsFromDb.Count = cartDetailDto.Count; //sự khác nhau với bulk
                            cartDetailDto.CartHeaderId = cartDetailsFromDb.CartHeaderId;
                            cartDetailDto.CartDetailsId = cartDetailsFromDb.CartDetailsId;
                            _db.CartDetails.Update(_mapper.Map<CartDetails>(cartDetailDto));
                        }
                    }
                    _response.Message = "Add Cart Successfully";
                    await _db.SaveChangesAsync();
                }
                _response.Result = cartDto;
            }
            catch (Exception ex)
            {
                _response.Message = ex.Message.ToString();
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDto> RemoveCart(string userId)
        {
            try
            {
                // Tìm CartHeader dựa trên UserId
                var cartHeader = await _db.CartHeaders.FirstOrDefaultAsync(ch => ch.UserId == userId);

                if (cartHeader == null)
                {
                    return new ResponseDto { Message = "Không tìm thấy CartHeader", IsSuccess = false };
                }

                // Tìm tất cả CartDetails có cùng CartHeaderId
                var cartDetailsToRemove = _db.CartDetails.Where(cd => cd.CartHeaderId == cartHeader.CartHeaderId);

                // Xóa tất cả CartDetails
                _db.CartDetails.RemoveRange(cartDetailsToRemove);

                // Xóa CartHeader
                _db.CartHeaders.Remove(cartHeader);

                // Lưu thay đổi vào cơ sở dữ liệu
                await _db.SaveChangesAsync();

                return new ResponseDto { Message = "Xóa thành công", IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseDto { Message = "Lỗi khi xóa Cart: " + ex.Message, IsSuccess = false };
            }
        }


        public async Task<ResponseDto> RemoveCartDetail(int cardHeaderId, int cartDetailId)
        {
            try
            { 
               
                // Tìm tất cả CartDetails có cùng CartHeaderId
                var cartDetailsToRemove =await _db.CartDetails.FirstOrDefaultAsync(cd => cd.CartHeaderId == cardHeaderId && cd.CartDetailsId == cartDetailId);

                // Xóa tất cả CartDetails
                _db.CartDetails.Remove(cartDetailsToRemove);

                // Lưu thay đổi vào cơ sở dữ liệu
                await _db.SaveChangesAsync();

                return new ResponseDto { Message = "Delete Product  Successful", IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseDto { Message = "Error delete product cart " , IsSuccess = false };
            }
        }
    }
}
