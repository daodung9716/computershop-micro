﻿using AutoMapper;
using Azure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProductAPI.Data;
using ProductAPI.Models;
using ProductAPI.Models.Dto;
using ProductAPI.Services.IService;

namespace ProductAPI.Services
{
    public class ProductService : IProductService
    {
        private readonly AppDbContext _db;
        private ResponseDto _response;
        private IMapper _mapper;

        public ProductService(AppDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
            _response = new ResponseDto();
        }


        public async Task<ResponseDto> UpsertCategory(IFormFile? imageFile, string categoryJson)
        {
            try
            {
                var category = JsonConvert.DeserializeObject<Category>(categoryJson);

                if (category.Id == null || category.Id == 0)
                {
                    var serverImagePath = "F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src\\assets\\images\\category";


                    if (imageFile != null)
                    {
                        var fileExtension = Path.GetExtension(imageFile.FileName);
                        // Tạo tên tệp hình ảnh bằng cách kết hợp tên danh mục và số bắt đầu từ 1
                        var fileName = Path.Combine(serverImagePath, category.Name.Replace(" ", "") + "1" + fileExtension);

                        // Kiểm tra xem tên tệp đã tồn tại chưa, nếu có thì tăng số lên
                        int counter = 1;
                        while (System.IO.File.Exists(fileName))
                        {
                            counter++;
                            fileName = Path.Combine(serverImagePath, category.Name.Replace(" ", "") + counter + fileExtension);
                        }

                        using (var stream = new FileStream(fileName, FileMode.Create))
                        {
                            await imageFile.CopyToAsync(stream);
                        }

                        var dtbSavepath = $"assets/images/category/";

                        // Lưu tên tệp hình ảnh vào thuộc tính Image của danh mục
                        category.Image = dtbSavepath + Path.GetFileName(fileName);

                    }
                    await _db.Categories.AddAsync(category);
                    _response.Message = "Add Category Successfully";
                }
                else
                {
                    var deleteImagePath = "F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src";
                    var createImagePath = "F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src\\assets\\images\\category";
                    var categoryToUpdate = _db.Categories.Find(category.Id);


                    if (categoryToUpdate != null)
                    {
                        if (imageFile != null)
                        {
                            if (!string.IsNullOrEmpty(categoryToUpdate.Image))
                            {
                                var currentImageFilePath = Path.Combine(deleteImagePath, categoryToUpdate.Image);
                                if (System.IO.File.Exists(currentImageFilePath))
                                {
                                    System.IO.File.Delete(currentImageFilePath);
                                }
                            }

                            // Xử lý tải lên tệp hình ảnh mới và lưu vào thư mục assets/images/category
                            var newFileName = Path.Combine(createImagePath, category.Name.Replace(" ", "") + "1" + Path.GetExtension(imageFile.FileName));
                            using (var stream = new FileStream(newFileName, FileMode.Create))
                            {
                                await imageFile.CopyToAsync(stream);
                            }

                            var dtbSavepath = $"assets/images/category/";

                            // Lưu tên tệp hình ảnh mới vào thuộc tính Image của danh mục
                            category.Image = dtbSavepath + Path.GetFileName(newFileName);

                        }
                    }
                    else
                    {
                        _response.Message = "No category in system";
                        _response.IsSuccess = false;
                        return _response;
                    }
                  
                    _db.Entry(categoryToUpdate).State = EntityState.Detached;
                    _db.Categories.Update(category);
                    _response.Message = "Update Category Successfully";
                }
                await _db.SaveChangesAsync();
            }
            catch
            {
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDto> UpsertProduct(List<IFormFile>? imageFiles, string productDtoJson)
        {
            try
            {
                var productDto = JsonConvert.DeserializeObject<ProductDto>(productDtoJson);

                var categoryName = _db.Categories.Where(x => x.Id == productDto.CategoryId).Select(x => x.Name).FirstOrDefault();

                var name = categoryName.ToLower();

                if (productDto.Id == null || productDto.Id == Guid.Empty)
                {
                    Product product = _mapper.Map<Product>(productDto);
                    product.Id = Guid.NewGuid();


                    await _db.Products.AddAsync(product);

                    var serverImagePath = $"F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src\\assets\\images\\products\\{name}";

                    if (imageFiles != null)
                    {
                        foreach (var imageFile in imageFiles)
                        {
                            var fileExtension = Path.GetExtension(imageFile.FileName);
                            // Tạo tên tệp hình ảnh bằng cách kết hợp tên danh mục và số bắt đầu từ 1
                            var fileName = Path.Combine(serverImagePath, productDto.Name.Replace(" ", "") + "1" + fileExtension);

                            // Kiểm tra xem tên tệp đã tồn tại chưa, nếu có thì tăng số lên
                            int counter = 1;
                            while (System.IO.File.Exists(fileName))
                            {
                                counter++;
                                fileName = Path.Combine(serverImagePath, productDto.Name.Replace(" ", "") + counter + fileExtension);
                            }

                            using (var stream = new FileStream(fileName, FileMode.Create))
                            {
                                await imageFile.CopyToAsync(stream);
                            }

                            //product image
                            var dtbSavepath = $"assets/images/products/{name}/";

                            var productImage = new ProductImage
                            {
                                Id = Guid.NewGuid(),
                                ImageUrl = dtbSavepath + Path.GetFileName(fileName),
                                ProductId = product.Id
                            };

                            await _db.ProductImages.AddAsync(productImage);

                        }

                    }
                    _response.Message = "Add Product Successfully";

                }
                else
                {
                    var deleteImagePath = "F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src";
                    var createImagePath = $"F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src\\assets\\images\\products\\{name}";
                    var productToUpdate = _db.Products.FirstOrDefault(p => p.Id == productDto.Id);
                    if (productToUpdate != null)
                    {
                        _mapper.Map(productDto, productToUpdate); // Cập nhật thuộc tính của productToUpdate từ productDto                      
                    }
                    else
                    {
                        _response.Message = "No product in system";
                        _response.IsSuccess = false;
                        return _response;
                    }

                    if (imageFiles != null)
                    {
                        foreach (var imageFile in imageFiles)
                        {
                            var fileExtension = Path.GetExtension(imageFile.FileName);
                            var newFileName = Path.Combine(createImagePath, productToUpdate.Name.Replace(" ", "") + "1" + fileExtension);

                            int counter = 1;
                            while (System.IO.File.Exists(newFileName))
                            {
                                counter++;
                                newFileName = Path.Combine(createImagePath, productToUpdate.Name.Replace(" ", "") + counter + fileExtension);
                            }

                            using (var stream = new FileStream(newFileName, FileMode.Create))
                            {
                                await imageFile.CopyToAsync(stream);
                            }

                            // Tạo mới thêm ảnh
                            var dtbSavepath = $"assets/images/products/{name}/";

                            var productImage = new ProductImage
                            {
                                Id = Guid.NewGuid(),
                                ImageUrl = dtbSavepath + Path.GetFileName(newFileName),
                                ProductId = productToUpdate.Id
                            };

                            await _db.ProductImages.AddAsync(productImage);
                            _response.Message = "Update Product Successfully";

                        }
                    }
                }
                await _db.SaveChangesAsync();
            }
            catch
            {
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDto> UpsertAuthor(Author author)
        {
            try
            {
                if (author.Id == null || author.Id == 0)
                {
                    _db.Authors.Add(author);
                    _response.Message = "Add Author Successfully";
                }
                else
                {
                    _db.Authors.Update(author);
                    _response.Message = "Update Author Successfully";
                }
                _db.SaveChanges();
            }
            catch
            {
                _response.IsSuccess = false;
            }
            return _response;
        }


        public async Task<ResponseDto> DeleteCategory(int id)
        {
            try
            {
                var categoryToDelete = _db.Categories.FirstOrDefault(u => u.Id == id);
                if (categoryToDelete == null)
                {
                    _response.Message = "Delete Category Failed";
                    _response.IsSuccess = false;
                }

                _response.Message = "Delete Category Successfully";
                _db.Categories.Remove(categoryToDelete);
                _db.SaveChanges();
            }
            catch
            {
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDto> DeleteAuthor(int id)
        {
            try
            {
                var authorToDelete = _db.Authors.FirstOrDefault(u => u.Id == id);
                if (authorToDelete == null)
                {
                    _response.Message = "Delete Authors Failed";
                    _response.IsSuccess = false;
                }
                _response.Message = "Delete Author Successfully";
                _db.Authors.Remove(authorToDelete);
                _db.SaveChanges();
            }
            catch
            {
                _response.IsSuccess = false;
            }
            return _response;
        }

      
        public async Task<ResponseDto> DeleteProduct(Guid productId)
        {
            try
            {
                List<ProductImage> productImage = _db.ProductImages.Where(x => x.ProductId == productId).ToList();
                if (productImage.Count > 0)
                {
                    _db.ProductImages.RemoveRange(productImage);
                }


                var deleteImagePath = "F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src";

                foreach (var item in productImage)
                {
                    if (!string.IsNullOrEmpty(item.ImageUrl))
                    {
                        var currentImageFilePath = Path.Combine(deleteImagePath, item.ImageUrl);
                        if (System.IO.File.Exists(currentImageFilePath))
                        {
                            System.IO.File.Delete(currentImageFilePath);
                        }
                    }
                }


                var productDelete = _db.Products.Find(productId);
                if (productDelete == null)
                {
                    _response.Message = "No product existed in system";
                    _response.IsSuccess = false;
                }
                _response.Message = "Delete Product Successfully";
                _db.Products.Remove(productDelete);
                _db.SaveChanges();
            }
            catch
            {
                _response.IsSuccess = false;
            }
            return _response;
        }

        public async Task<ResponseDto> DeleteProductImage(Guid id)
        {
            try
            {
                var deleteImagePath = "F:\\hoc tap\\11 nextproject\\FE\\GIT FE\\ecommerce\\src";
                ProductImage productImage = _db.ProductImages.Where(x => x.Id == id).FirstOrDefault();


                if (!string.IsNullOrEmpty(productImage.ImageUrl))
                {
                    var currentImageFilePath = Path.Combine(deleteImagePath, productImage.ImageUrl);
                    if (System.IO.File.Exists(currentImageFilePath))
                    {
                        System.IO.File.Delete(currentImageFilePath);
                    }
                }

                _db.ProductImages.Remove(productImage);
                _db.SaveChanges();
                _response.Message = "Delete ProductImage Successful";
            }

            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

      


    }
}
