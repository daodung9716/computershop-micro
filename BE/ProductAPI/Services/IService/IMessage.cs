﻿namespace ProductAPI.Services.IService
{
    public interface IMessage
    {
        public void SendingMessage<T>(T message);
    }
}
