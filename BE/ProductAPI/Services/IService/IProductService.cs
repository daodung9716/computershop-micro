﻿using Microsoft.AspNetCore.Mvc;
using ProductAPI.Models;
using ProductAPI.Models.Dto;

namespace ProductAPI.Services.IService
{
    public interface IProductService
    {
        Task<ResponseDto> UpsertCategory(IFormFile? imageFile, string categoryJson);

        Task<ResponseDto> UpsertProduct(List<IFormFile>? imageFiles, string productDtoJson);

        Task<ResponseDto> UpsertAuthor(Author author);

        Task<ResponseDto> DeleteCategory(int id);

        Task<ResponseDto> DeleteAuthor(int id);

        Task<ResponseDto> DeleteProduct(Guid productId);

        Task<ResponseDto> DeleteProductImage(Guid id);






    }
}
