﻿using ProductAPI.Models.Dto;

namespace ProductAPI.Services.IService
{
    public interface IGetService
    {
        Task<ResponseDto> GetProduct(int page, string? searchField);

        Task<ResponseDto> GetAllProduct();

        Task<ResponseDto> GetAllCategory();

        Task<ResponseDto> GetAllAuthor();
        Task<ResponseDto> GetByIdProduct(Guid id);

        Task<ResponseDto> GetRecommend();


        Task<ResponseDto> GetBestBuy();

        Task<ResponseDto> GetRelate(Guid id);

        Task<ResponseDto> GetCategory(int page, string? searchField);

        Task<ResponseDto> GetAuthor(int page, string? searchField);

        Task<ResponseDto> GetProductByCategory(int id);

        Task<ResponseDto> GetProductByAuthor(int id);

        Task<ResponseDto> GetProductByAuthorCategory(int? idAuthor, int? idCategory, int? minPrice, int? maxPrice);


        Task<ResponseDto> GetByIdCategory(int id);


        Task<ResponseDto> GetByIdAuthor(int id);


    }
}
