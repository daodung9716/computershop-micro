﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProductAPI.Data;
using ProductAPI.Models;
using ProductAPI.Models.Dto;
using ProductAPI.Services.IService;

namespace ProductAPI.Services
{
    public class GetService : IGetService
    {
        private readonly AppDbContext _db;
        private ResponseDto _response;
        private IMapper _mapper;

        public GetService(AppDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
            _response = new ResponseDto();
        }

        public async Task<ResponseDto> GetProduct(int page, string? searchField)
        {
            int pageSize = 10;
            try
            {
                var query = await (from product in _db.Products.AsNoTracking()
                                   join category in _db.Categories.AsNoTracking() on product.CategoryId equals category.Id into b1
                                   from b2 in b1.DefaultIfEmpty()
                                   join author in _db.Authors.AsNoTracking() on product.AuthorId equals author.Id into c1
                                   from c2 in c1.DefaultIfEmpty()
                                   where (string.IsNullOrEmpty(searchField)) || product.Name.ToLower().Contains(searchField.ToLower())
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       CategoryName = b2.Name,
                                       AuthorName = c2.Name,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();

                _response.PageSize = pageSize;
                _response.TotalPages = (int)Math.Ceiling((double)query.Count() / pageSize);
                var number = query.Skip((page - 1) * pageSize).Take(pageSize);
                _response.Result = number;
                _response.RecordPerPage = number.Count();
                _response.RecordsTotal = query.Count();


            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetAllProduct()
        {
            try
            {
                // Lấy tất cả sản phẩm cùng danh mục với productId
                var query = await (from product in _db.Products.AsNoTracking()
                                   join category in _db.Categories.AsNoTracking() on product.CategoryId equals category.Id into b1
                                   from b2 in b1.DefaultIfEmpty()
                                   join author in _db.Authors.AsNoTracking() on product.AuthorId equals author.Id into c1
                                   from c2 in c1.DefaultIfEmpty()
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into d1

                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       Description = product.Description,
                                       AuthorName = c2.Name,
                                       CategoryName = b2.Name,
                                       ImageUrl = d1.OrderBy(pi => pi.Id).FirstOrDefault().ImageUrl,

                                   }).ToListAsync();


                _response.Result = query;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetByIdProduct(Guid id)
        {
            try
            {
                var query = await (from product in _db.Products.AsNoTracking()
                                   join category in _db.Categories.AsNoTracking() on product.CategoryId equals category.Id into b1
                                   from b2 in b1.DefaultIfEmpty()
                                   join author in _db.Authors.AsNoTracking() on product.AuthorId equals author.Id into c1
                                   from c2 in c1.DefaultIfEmpty()
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into d1
                                   where product.Id == id

                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       Description = product.Description,
                                       AuthorName = c2.Name,
                                       AuthorId = c2.Id,
                                       CategoryId = b2.Id,
                                       CategoryName = b2.Name,
                                       ImageUrl = d1.OrderBy(pi => pi.Id).FirstOrDefault().ImageUrl,
                                       ProductImages =
                                       (from a in _db.ProductImages.AsNoTracking()
                                        where a.ProductId == id
                                        select new ProductImageDto
                                        {
                                            Id = a.Id,
                                            ImageUrl = a.ImageUrl
                                        }).ToList()

                                   }).FirstOrDefaultAsync();

                _response.Result = query;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetRecommend()
        {
            try
            {
                var query = await (from product in _db.Products.AsNoTracking()
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       Description = product.Description,
                                       BuyCount = product.BuyCount,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();

                // Kiểm tra xem danh sách có ít hơn 4 sản phẩm hay không
                if (query.Count <= 4)
                {
                    _response.Result = query;
                }
                else
                {
                    // Lấy 4 sản phẩm ngẫu nhiên
                    Random random = new Random();
                    List<ProductVM> randomProducts = query.OrderBy(x => random.Next()).Take(4).ToList();

                    _response.Result = randomProducts;
                }

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetBestBuy()
        {
            try
            {
                var query = await (from product in _db.Products.AsNoTracking()
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       Description = product.Description,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();

                var result = query.OrderByDescending(p => p.BuyCount).Take(4);

                _response.Result = result;


            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        public async Task<ResponseDto> GetRelate(Guid id)
        {
            try
            {
                // Lấy danh mục của sản phẩm với productId
                var productCategory = await _db.Products.Where(p => p.Id == id).AsNoTracking().Select(p => p.CategoryId).FirstOrDefaultAsync();

                if (productCategory == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Danh mục sản phẩm không tồn tại";
                    return _response;
                }

                // Lấy tất cả sản phẩm cùng danh mục với productId
                var query = await (from product in _db.Products.AsNoTracking()
                                   where product.CategoryId == productCategory
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       Description = product.Description,
                                       BuyCount = product.BuyCount,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();

                var result = query.OrderByDescending(p => p.BuyCount).Take(4);
                _response.Result = result;

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetCategory(int page, string? searchField)
        {
            int pageSize = 10;
            IEnumerable<Category> objList;
            try
            {
                if (string.IsNullOrEmpty(searchField))
                {
                    objList = await _db.Categories.AsNoTracking().ToListAsync();
                }
                else
                {
                    objList = await _db.Categories.AsNoTracking().Where(x => x.Name.ToLower().Contains(searchField.ToLower())).ToListAsync();
                }

                _response.PageSize = pageSize;
                _response.TotalPages = (int)Math.Ceiling((double)objList.Count() / pageSize);
                var number = objList.Skip((page - 1) * pageSize).Take(pageSize);
                _response.Result = number;
                _response.RecordPerPage = number.Count();
                _response.RecordsTotal = objList.Count();
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetAllCategory()
        {
        
            try
            {
                IEnumerable<Category> objList = await _db.Categories.AsNoTracking().ToListAsync();
                _response.Result = objList;

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        public async Task<ResponseDto> GetAllAuthor()
        {

            try
            {
                IEnumerable<Author> objList = await _db.Authors.AsNoTracking().ToListAsync();
                _response.Result = objList;

            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetAuthor(int page, string? searchField)
        {
            int pageSize = 10;
            IEnumerable<Author> objList;
            try
            {
                if (string.IsNullOrEmpty(searchField))
                {
                    objList = await _db.Authors.AsNoTracking().ToListAsync();
                }
                else
                {
                    objList = await _db.Authors.AsNoTracking().Where(x => x.Name.ToLower().Contains(searchField.ToLower())).ToListAsync();
                }
                _response.PageSize = pageSize;
                _response.TotalPages = (int)Math.Ceiling((double)objList.Count() / pageSize);
                var number = objList.Skip((page - 1) * pageSize).Take(pageSize);
                _response.Result = number;
                _response.RecordPerPage = number.Count();
                _response.RecordsTotal = objList.Count();
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }


        public async Task<ResponseDto> GetProductByCategory(int id)
        {
            try
            {
                var productCategory = await _db.Categories.Where(p => p.Id == id).AsNoTracking().FirstOrDefaultAsync();

                if (productCategory == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Category không tồn tại";
                    return _response;
                }

                // Lấy tất cả sản phẩm cùng Category
                var query = await (from product in _db.Products.AsNoTracking()
                                   where product.CategoryId == id
                                   join productImage in _db.ProductImages.AsNoTracking() on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();

                _response.Result = query;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }


        public async Task<ResponseDto> GetProductByAuthor(int id)
        {
            try
            {
                var productAuthor = await _db.Authors.Where(p => p.Id == id).AsNoTracking().FirstOrDefaultAsync();

                if (productAuthor == null)
                {
                    _response.IsSuccess = false;
                    _response.Message = "Author không tồn tại";
                    return _response;
                }

                // Lấy tất cả sản phẩm cùng Category
                var query = await (from product in _db.Products
                                   where product.AuthorId == id
                                   join productImage in _db.ProductImages on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();

                _response.Result = query;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDto> GetProductByAuthorCategory(int? idAuthor, int? idCategory, int? minPrice, int? maxPrice)
        {
            try
            {
                var query = await (from product in _db.Products.AsNoTracking()
                                   where (idAuthor == null || product.AuthorId == idAuthor) &&
                                         (idCategory == null || product.CategoryId == idCategory) &&
                                         (product.AuthorId == idAuthor || product.CategoryId == idCategory) &&
                                         (minPrice == null || product.Price >= minPrice) && (maxPrice == null || product.Price <= maxPrice)
                                   join productImage in _db.ProductImages on product.Id equals productImage.ProductId into productImagesGroup
                                   select new ProductVM
                                   {
                                       Id = product.Id,
                                       Name = product.Name,
                                       Price = product.Price,
                                       ListPrice = product.ListPrice,
                                       BuyCount = product.BuyCount,
                                       ImageUrl = productImagesGroup.OrderBy(pi => pi.ImageUrl).Select(pi => pi.ImageUrl).FirstOrDefault()
                                   }).ToListAsync();


                _response.Result = query;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }

            return _response;
        }

        public async Task<ResponseDto> GetByIdCategory(int id)
        {
            try
            {
                Category obj = await _db.Categories.AsNoTracking().FirstAsync(u => u.Id == id);
                _response.Result = obj;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }

        public async Task<ResponseDto> GetByIdAuthor(int id)
        {
            try
            {
                Author obj = await _db.Authors.AsNoTracking().FirstAsync(u => u.Id == id);
                _response.Result = obj;
            }
            catch (Exception ex)
            {
                _response.IsSuccess = false;
                _response.Message = ex.Message;
            }
            return _response;
        }
    }
}
