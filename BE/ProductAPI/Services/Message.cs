﻿using ProductAPI.Services.IService;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json;

namespace ProductAPI.Services
{
    public class Message : IMessage
    {
        public void SendingMessage<T>(T message)
        {
           
                var factory = new ConnectionFactory()
                {
                    HostName = "host.docker.internal",
                    UserName = "Admin",
                    Password = "123456",
                    VirtualHost = "/"
                };

                var connection = factory.CreateConnection();

                using var channel = connection.CreateModel();

                channel.QueueDeclare("products", durable: false, exclusive: false);

                var jsonString = JsonSerializer.Serialize(message);

                var body = Encoding.UTF8.GetBytes(jsonString);

                channel.BasicPublish(exchange: "", routingKey: "products", body: body);

           
        }
    }
}
