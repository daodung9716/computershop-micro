﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using ProductAPI.Models;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace ProductAPI.Data
{
    public class AppDbContext : DbContext
    {
       

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
           
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Author> Authors { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<ProductImage> ProductImages { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            #region Author
            modelBuilder.Entity<Author>().HasData(
               new Author { Id = 1, Name = "AMD" },
               new Author { Id = 2, Name = "INTEL" },
               new Author { Id = 3, Name = "MSI" },
               new Author { Id = 4, Name = "ASROCK" },
               new Author { Id = 5, Name = "ASUS" },
               new Author { Id = 6, Name = "GIGABYTE" },
               new Author { Id = 7, Name = "HKC" },
               new Author { Id = 8, Name = "AOC" },
               new Author { Id = 9, Name = "E-DRA" },
               new Author { Id = 10, Name = "DAREU" },
                 new Author { Id = 11, Name = "CORSAIR" }

               );
            #endregion

            #region Category
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "Mainboard", DisplayOrder = 1,Image= "assets/images/category/category-11.jpg" },
                new Category { Id = 2, Name = "CPU", DisplayOrder = 2,Image = "assets/images/category/category-22.jpg" },
                new Category { Id = 3, Name = "VGA", DisplayOrder = 3,Image= "assets/images/category/category-33.jpg" },
                new Category { Id = 4, Name = "Monitor", DisplayOrder = 4,Image= "assets/images/category/category-44.jpg" },
                new Category { Id = 5, Name = "Keyboard", DisplayOrder = 5,Image= "assets/images/category/category-55.jpg" },
                new Category { Id = 6, Name = "Mouse", DisplayOrder = 6,Image= "assets/images/category/category-66.jpg" }
                );
            #endregion


            #region Product
            modelBuilder.Entity<Product>().HasData(

          //CPU

          new Product
          {
              Id = Guid.Parse("88142B2B-D710-46C1-87B0-CA4FB9E8EE6E"),
              Name = "RYZEN 3 3100",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              BuyCount = 40,
              ListPrice = 50,
              Price = 43,
              CategoryId = 2,

          },
          new Product
          {
              Id = Guid.Parse("8361D4AF-7BE2-4F20-B544-D54C1D149F90"),
              Name = "RYZEN 5 3600",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              BuyCount = 20,
              ListPrice = 100,
              Price = 85,
              CategoryId = 2,


          },
          new Product
          {
              Id = Guid.Parse("EB5F667A-201A-4727-A367-EDC3F914B32D"),
              Name = "RYZEN 5 5600X",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              ListPrice = 150,
              Price = 120,
              BuyCount = 30,
              CategoryId = 2,
          },
          new Product
          {
              Id = Guid.Parse("B0AC6BBC-3C5A-4787-89AC-34B43B1EAA61"),
              Name = "RYZEN 9 5900X",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              ListPrice = 300,
              Price = 275,
              CategoryId = 2,
              BuyCount = 40,
          },
            //MAIN
          new Product
          {
              Id = Guid.Parse("33005AD9-DDD8-4DBB-B025-669D781F4B5F"),
              Name = "A520M PRO",
              AuthorId = 3,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              ListPrice = 50,
              Price = 43,
              CategoryId = 1,
              BuyCount = 10,
          },
          new Product
          {
              Id = Guid.Parse("4271654F-FA30-4A61-89BB-561B61D37EB7"),
              Name = "B550M PRO VDH WIFI",
              AuthorId = 3,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              ListPrice = 100,
              Price = 85,
              BuyCount = 10,
              CategoryId = 1,

          },
          new Product
          {
              Id = Guid.Parse("A180A819-51E9-46C9-B4E0-6E406A6FC658"),
              Name = "X570 TUF",
              AuthorId = 5,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              BuyCount = 10,
              ListPrice = 150,
              Price = 120,
              CategoryId = 1,
          },
          new Product
          {
              Id = Guid.Parse("33ED75DE-1F7F-41AA-BB3D-FE2AB5F9D265"),
              Name = "X570 ACE",
              AuthorId = 3,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              ListPrice = 300,
              Price = 275,
              CategoryId = 1,
              BuyCount = 20,
          },

        

            //VGA

            new Product
            {
                Id = Guid.Parse("380825B1-2650-4B48-8CC2-D9C35A509BA8"),
                Name = "RX 550",
                AuthorId = 1,
                Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
                ListPrice = 50,
                Price = 43,
                BuyCount = 50,
                CategoryId = 3,

            },
          new Product
          {
              Id =  Guid.Parse("1DB6C932-E9EE-4FFD-BBF0-92EF23061982"),
              Name = "RX 570",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
            
              ListPrice = 100,
              Price = 85,
              BuyCount = 60,
              CategoryId = 3,


          },
          new Product
          {
              Id = Guid.Parse("1FC51323-7607-4A7F-AAF7-05F09B0B3DFB"),
              Name = "RX 6600XT",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",           
              ListPrice = 150,
              Price = 120,
              BuyCount = 10,
              CategoryId = 3,
          },
          new Product
          {
              Id = Guid.Parse("EA4D6B02-3EEE-430A-A160-E4D5F5C7C943"),
              Name = "RX 6800XT",
              AuthorId = 1,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
             
              ListPrice = 300,
              Price = 275,
              BuyCount = 10,
              CategoryId = 3,
          },
         // VGA



         // Monitor

            new Product
            {
                Id = Guid.Parse("C273F848-A068-42B4-9BAF-382079BB52A1"),
                Name = "HKC ANT-22F220 (22/FHD/75Hz/1ms)",
                AuthorId = 7,
                Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
               
                ListPrice = 50,
                Price = 43,
                BuyCount = 10,
                CategoryId = 4,

            },
          new Product
          {
              Id = Guid.Parse("401DA5D4-6F94-44EA-A4C7-3489DB8056E8"),
              Name = "E-DRA EGM24F75 (23.8/FHD/IPS/75Hz/1ms) ",
              AuthorId = 9,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
             
              ListPrice = 100,
              Price = 85,
              BuyCount = 10,
              CategoryId = 4,


          },
          new Product
          {
              Id = Guid.Parse("B1317290-360B-4984-B45A-3B8138A11B39"),
              Name = "Asus VY249HE (23.8/FHD/IPS/75Hz/1ms) ",
              AuthorId = 5,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
           
              ListPrice = 150,
              Price = 120,
              BuyCount = 80,
              CategoryId = 4,
          },
          new Product
          {
              Id = Guid.Parse("8B614A6D-52E9-4093-9948-00CCD45CA6D8"),
              Name = "AOC CQ32G3SE/74 (31.5/QHD/VA/165HZ/1MS) ",
              AuthorId = 8,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
             
              ListPrice = 300,
              Price = 275,
              BuyCount = 60,
              CategoryId = 4,
          },
         // Monitor


         // Keyboard

          new Product
          {
              Id = Guid.Parse("04E7318D-61BB-455D-931B-4BAF081E8F99"),
              Name = " DAREU EK87 - Black",
              AuthorId = 10,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
            
              ListPrice = 25,
              Price = 21,
              BuyCount = 40,
              CategoryId = 5,

          },
          new Product
          {
              Id = Guid.Parse("9CBCC2E0-0DBF-437F-92C7-C18E96852A2D"),
              Name = " EK810 Pink (USB/Pink Led/Blue/Brown/Red switch)  ",
              AuthorId = 10,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
           
              ListPrice = 35,
              Price = 28,
              BuyCount = 10,
              CategoryId = 5,



          },
          new Product
          {
              Id = Guid.Parse("3EF348A2-D648-4563-AA0E-E6569BC93D84"),
              Name = " DAREU EK8100 100KEY (RGB, Blue/ Brown/ Red D switch)  ",
              AuthorId = 10,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
           
              ListPrice = 43,
              Price = 40,
              BuyCount = 30,
              CategoryId = 5,

          },
          new Product
          {
              Id = Guid.Parse("091D512E-9706-4D9B-9823-58EB7F83F118"),
              Name = "DAREU A87 SWALLOW (PBT, Brown/ Red CHERRY switch) ",
              AuthorId = 10,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
           
              ListPrice = 125,
              Price = 121,
              BuyCount = 60,
              CategoryId = 5,

          },
          //Keyboard


          //Mouse

            new Product
            {
                Id = Guid.Parse("5B3D98F9-EFA7-4503-A9B1-FBDE14FC94DE"),
                Name = " DAREU A918 - BLACK (PixArt PMW3335)",
                AuthorId = 10,
                Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
              
                ListPrice = 30,
                Price = 26,
                BuyCount = 70,
                CategoryId = 6,

            },
          new Product
          {
              Id = Guid.Parse("38AED6B0-AB77-4D79-83FF-A9D3ADC46758"),
              Name = " DareU EM901X RGB Superlight  ",
              AuthorId = 10,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
            
              ListPrice = 25,
              Price = 21,
              BuyCount = 80,
              CategoryId = 6,



          },
          new Product
          {
              Id = Guid.Parse("DD8D66E3-3D59-415C-8E82-E727B3E215BD"),
              Name = " DAREU A960 YELLOW - ULTRALIGHT ",
              AuthorId = 10,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
            
              ListPrice = 43,
              Price = 40,
              BuyCount = 90,
              CategoryId = 6,

          },
          new Product
          {
              Id = Guid.Parse("457A2725-0870-4D56-9706-A5FD11A83FC1"),
              Name = "Corsair M65 RGB ULTRA Black",
              AuthorId = 11,
              Description = "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ",
             
              ListPrice = 125,
              Price = 121,
              BuyCount = 10,
              CategoryId = 6,

          }

         // Mouse

          ) ;
            #endregion


            #region ProductImage
            var productImages = new List<ProductImage>
            {
                    new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/cpu/3100.jpg", ProductId = Guid.Parse("88142B2B-D710-46C1-87B0-CA4FB9E8EE6E")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/cpu/3600.jpg", ProductId = Guid.Parse("8361D4AF-7BE2-4F20-B544-D54C1D149F90")},
                  new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/cpu/3600.jpg", ProductId = Guid.Parse("EB5F667A-201A-4727-A367-EDC3F914B32D")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/cpu/3600.jpg", ProductId = Guid.Parse("B0AC6BBC-3C5A-4787-89AC-34B43B1EAA61")},


                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/a520m1.jpg", ProductId = Guid.Parse("33005AD9-DDD8-4DBB-B025-669D781F4B5F")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/a520m2.jpg", ProductId = Guid.Parse("33005AD9-DDD8-4DBB-B025-669D781F4B5F")},
                 new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/b550m1.jpg", ProductId = Guid.Parse("4271654F-FA30-4A61-89BB-561B61D37EB7")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/b550m2.jpg", ProductId = Guid.Parse("4271654F-FA30-4A61-89BB-561B61D37EB7")},
                 new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/x570tuf.jpg", ProductId = Guid.Parse("A180A819-51E9-46C9-B4E0-6E406A6FC658")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/x570tuf2.jpg", ProductId = Guid.Parse("A180A819-51E9-46C9-B4E0-6E406A6FC658")},
                     new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/x570ace.jpg", ProductId = Guid.Parse("33ED75DE-1F7F-41AA-BB3D-FE2AB5F9D265")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/main/x570ace2.jpg", ProductId = Guid.Parse("33ED75DE-1F7F-41AA-BB3D-FE2AB5F9D265")},



            new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/monitor/aoc32.jpg", ProductId = Guid.Parse("8B614A6D-52E9-4093-9948-00CCD45CA6D8")},
               new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/monitor/asus24vy.jpg", ProductId = Guid.Parse("B1317290-360B-4984-B45A-3B8138A11B39")},
              new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/monitor/edra24.jpg", ProductId = Guid.Parse("401DA5D4-6F94-44EA-A4C7-3489DB8056E8")},
             new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/monitor/hkcant.jpg", ProductId = Guid.Parse("C273F848-A068-42B4-9BAF-382079BB52A1")},

              new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/vga/550m1.jpg", ProductId = Guid.Parse("380825B1-2650-4B48-8CC2-D9C35A509BA8")},
               new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/vga/550m2.jpg", ProductId = Guid.Parse("380825B1-2650-4B48-8CC2-D9C35A509BA8")},
                new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/vga/570m1.jpg", ProductId = Guid.Parse("1DB6C932-E9EE-4FFD-BBF0-92EF23061982")},
                 new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/vga/570m2.jpg", ProductId = Guid.Parse("1DB6C932-E9EE-4FFD-BBF0-92EF23061982")},
                  new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/vga/rx6600xt1.jpg", ProductId = Guid.Parse("1FC51323-7607-4A7F-AAF7-05F09B0B3DFB")},
                   new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/vga/rx6800xtm.jpg", ProductId = Guid.Parse("EA4D6B02-3EEE-430A-A160-E4D5F5C7C943")},

                     new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/keyboard/dareua87.jpg", ProductId = Guid.Parse("091D512E-9706-4D9B-9823-58EB7F83F118")},
                     new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/keyboard/dareuek810.jpg", ProductId = Guid.Parse("9CBCC2E0-0DBF-437F-92C7-C18E96852A2D")},
                     new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/keyboard/ek87.jpg", ProductId = Guid.Parse("04E7318D-61BB-455D-931B-4BAF081E8F99")},
                     new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/keyboard/ek8100.jpg", ProductId = Guid.Parse("3EF348A2-D648-4563-AA0E-E6569BC93D84")},

                     new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/mouse/a918black.jpg", ProductId = Guid.Parse("5B3D98F9-EFA7-4503-A9B1-FBDE14FC94DE")},
                      new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/mouse/a960y.jpg", ProductId = Guid.Parse("DD8D66E3-3D59-415C-8E82-E727B3E215BD")},
                       new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/mouse/em901xm.jpg", ProductId = Guid.Parse("38AED6B0-AB77-4D79-83FF-A9D3ADC46758")},
                        new ProductImage { Id = Guid.NewGuid(), ImageUrl = "assets/images/products/mouse/m65rgb.jpg", ProductId = Guid.Parse("457A2725-0870-4D56-9706-A5FD11A83FC1")},
            };

            modelBuilder.Entity<ProductImage>().HasData(productImages);
            #endregion


        }
    }
}
