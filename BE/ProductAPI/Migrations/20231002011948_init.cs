﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace ProductAPI.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ImageLocalPath = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DisplayOrder = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<double>(type: "float", nullable: false),
                    ListPrice = table.Column<double>(type: "float", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BuyCount = table.Column<int>(type: "int", nullable: true),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Products_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Products_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProductImages",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProductId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductImages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductImages_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "AMD" },
                    { 2, "INTEL" },
                    { 3, "MSI" },
                    { 4, "ASROCK" },
                    { 5, "ASUS" },
                    { 6, "GIGABYTE" },
                    { 7, "HKC" },
                    { 8, "AOC" },
                    { 9, "E-DRA" },
                    { 10, "DAREU" },
                    { 11, "CORSAIR" }
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "DisplayOrder", "Image", "ImageLocalPath", "Name" },
                values: new object[,]
                {
                    { 1, 1, "assets/images/category/category-11.jpg", null, "Mainboard" },
                    { 2, 2, "assets/images/category/category-22.jpg", null, "CPU" },
                    { 3, 3, "assets/images/category/category-33.jpg", null, "VGA" },
                    { 4, 4, "assets/images/category/category-44.jpg", null, "Monitor" },
                    { 5, 5, "assets/images/category/category-55.jpg", null, "Keyboard" },
                    { 6, 6, "assets/images/category/category-66.jpg", null, "Mouse" }
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "AuthorId", "BuyCount", "CategoryId", "Description", "ImageUrl", "ListPrice", "Name", "Price" },
                values: new object[,]
                {
                    { new Guid("04e7318d-61bb-455d-931b-4baf081e8f99"), 10, 40, 5, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 25.0, " DAREU EK87 - Black", 21.0 },
                    { new Guid("091d512e-9706-4d9b-9823-58eb7f83f118"), 10, 60, 5, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 125.0, "DAREU A87 SWALLOW (PBT, Brown/ Red CHERRY switch) ", 121.0 },
                    { new Guid("1db6c932-e9ee-4ffd-bbf0-92ef23061982"), 1, 60, 3, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 100.0, "RX 570", 85.0 },
                    { new Guid("1fc51323-7607-4a7f-aaf7-05f09b0b3dfb"), 1, 10, 3, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 150.0, "RX 6600XT", 120.0 },
                    { new Guid("33005ad9-ddd8-4dbb-b025-669d781f4b5f"), 3, 10, 1, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 50.0, "A520M PRO", 43.0 },
                    { new Guid("33ed75de-1f7f-41aa-bb3d-fe2ab5f9d265"), 3, 20, 1, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 300.0, "X570 ACE", 275.0 },
                    { new Guid("380825b1-2650-4b48-8cc2-d9c35a509ba8"), 1, 50, 3, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 50.0, "RX 550", 43.0 },
                    { new Guid("38aed6b0-ab77-4d79-83ff-a9d3adc46758"), 10, 80, 6, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 25.0, " DareU EM901X RGB Superlight  ", 21.0 },
                    { new Guid("3ef348a2-d648-4563-aa0e-e6569bc93d84"), 10, 30, 5, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 43.0, " DAREU EK8100 100KEY (RGB, Blue/ Brown/ Red D switch)  ", 40.0 },
                    { new Guid("401da5d4-6f94-44ea-a4c7-3489db8056e8"), 9, 10, 4, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 100.0, "E-DRA EGM24F75 (23.8/FHD/IPS/75Hz/1ms) ", 85.0 },
                    { new Guid("4271654f-fa30-4a61-89bb-561b61d37eb7"), 3, 10, 1, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 100.0, "B550M PRO VDH WIFI", 85.0 },
                    { new Guid("457a2725-0870-4d56-9706-a5fd11a83fc1"), 11, 10, 6, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 125.0, "Corsair M65 RGB ULTRA Black", 121.0 },
                    { new Guid("5b3d98f9-efa7-4503-a9b1-fbde14fc94de"), 10, 70, 6, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 30.0, " DAREU A918 - BLACK (PixArt PMW3335)", 26.0 },
                    { new Guid("8361d4af-7be2-4f20-b544-d54c1d149f90"), 1, 20, 2, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 100.0, "RYZEN 5 3600", 85.0 },
                    { new Guid("88142b2b-d710-46c1-87b0-ca4fb9e8ee6e"), 1, 40, 2, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 50.0, "RYZEN 3 3100", 43.0 },
                    { new Guid("8b614a6d-52e9-4093-9948-00ccd45ca6d8"), 8, 60, 4, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 300.0, "AOC CQ32G3SE/74 (31.5/QHD/VA/165HZ/1MS) ", 275.0 },
                    { new Guid("9cbcc2e0-0dbf-437f-92c7-c18e96852a2d"), 10, 10, 5, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 35.0, " EK810 Pink (USB/Pink Led/Blue/Brown/Red switch)  ", 28.0 },
                    { new Guid("a180a819-51e9-46c9-b4e0-6e406a6fc658"), 5, 10, 1, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 150.0, "X570 TUF", 120.0 },
                    { new Guid("b0ac6bbc-3c5a-4787-89ac-34b43b1eaa61"), 1, 40, 2, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 300.0, "RYZEN 9 5900X", 275.0 },
                    { new Guid("b1317290-360b-4984-b45a-3b8138a11b39"), 5, 80, 4, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 150.0, "Asus VY249HE (23.8/FHD/IPS/75Hz/1ms) ", 120.0 },
                    { new Guid("c273f848-a068-42b4-9baf-382079bb52a1"), 7, 10, 4, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 50.0, "HKC ANT-22F220 (22/FHD/75Hz/1ms)", 43.0 },
                    { new Guid("dd8d66e3-3d59-415c-8e82-e727b3e215bd"), 10, 90, 6, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 43.0, " DAREU A960 YELLOW - ULTRALIGHT ", 40.0 },
                    { new Guid("ea4d6b02-3eee-430a-a160-e4d5f5c7c943"), 1, 10, 3, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 300.0, "RX 6800XT", 275.0 },
                    { new Guid("eb5f667a-201a-4727-a367-edc3f914b32d"), 1, 30, 2, "Praesent vitae sodales libero. Praesent molestie orci augue, vitae euismod velit sollicitudin ac. Praesent vestibulum facilisis nibh ut ultricies.\r\n\r\nNunc malesuada viverra ipsum sit amet tincidunt. ", null, 150.0, "RYZEN 5 5600X", 120.0 }
                });

            migrationBuilder.InsertData(
                table: "ProductImages",
                columns: new[] { "Id", "ImageUrl", "ProductId" },
                values: new object[,]
                {
                    { new Guid("00a85922-93a2-415f-880b-53a954701ac0"), "assets/images/products/monitor/hkcant.jpg", new Guid("c273f848-a068-42b4-9baf-382079bb52a1") },
                    { new Guid("0430885f-1d86-45e4-ba3b-1921e85c9c83"), "assets/images/products/keyboard/ek87.jpg", new Guid("04e7318d-61bb-455d-931b-4baf081e8f99") },
                    { new Guid("0f18c69c-ab61-4c67-9d2b-7afeeea6da91"), "assets/images/products/keyboard/dareua87.jpg", new Guid("091d512e-9706-4d9b-9823-58eb7f83f118") },
                    { new Guid("212678ba-c0f0-48bb-ac1f-184475ff9125"), "assets/images/products/keyboard/ek8100.jpg", new Guid("3ef348a2-d648-4563-aa0e-e6569bc93d84") },
                    { new Guid("2b426d29-83f6-42fd-9ffa-0ab00b3d5eb2"), "assets/images/products/mouse/m65rgb.jpg", new Guid("457a2725-0870-4d56-9706-a5fd11a83fc1") },
                    { new Guid("2e8359eb-b2be-429d-abd3-81e640b8fb3b"), "assets/images/products/mouse/em901xm.jpg", new Guid("38aed6b0-ab77-4d79-83ff-a9d3adc46758") },
                    { new Guid("3077d477-6ef0-4e2c-acd7-90918561ccd3"), "assets/images/products/main/x570ace2.jpg", new Guid("33ed75de-1f7f-41aa-bb3d-fe2ab5f9d265") },
                    { new Guid("313aad70-566f-4643-979d-d534ff06d2fe"), "assets/images/products/main/a520m2.jpg", new Guid("33005ad9-ddd8-4dbb-b025-669d781f4b5f") },
                    { new Guid("360a7d1a-98a0-41d8-81c5-ae966ac00a75"), "assets/images/products/monitor/aoc32.jpg", new Guid("8b614a6d-52e9-4093-9948-00ccd45ca6d8") },
                    { new Guid("43eabbb2-2b70-406d-b00a-d21f300be646"), "assets/images/products/main/b550m2.jpg", new Guid("4271654f-fa30-4a61-89bb-561b61d37eb7") },
                    { new Guid("48539064-b68d-4b22-a687-27db1f51e5f5"), "assets/images/products/main/x570tuf2.jpg", new Guid("a180a819-51e9-46c9-b4e0-6e406a6fc658") },
                    { new Guid("4b850e16-2536-4f35-b692-d7b6d17fcd76"), "assets/images/products/cpu/3600.jpg", new Guid("b0ac6bbc-3c5a-4787-89ac-34b43b1eaa61") },
                    { new Guid("4ba0b460-02a2-4e06-89e5-8be45dfc00ef"), "assets/images/products/vga/rx6800xtm.jpg", new Guid("ea4d6b02-3eee-430a-a160-e4d5f5c7c943") },
                    { new Guid("6b4a0b3c-4155-4631-9e7b-bf18907a7ca0"), "assets/images/products/main/b550m1.jpg", new Guid("4271654f-fa30-4a61-89bb-561b61d37eb7") },
                    { new Guid("74284229-1822-4351-878d-fd507f9e80f4"), "assets/images/products/monitor/asus24vy.jpg", new Guid("b1317290-360b-4984-b45a-3b8138a11b39") },
                    { new Guid("7d5102c1-ce31-49b5-b921-03cb7f40437b"), "assets/images/products/cpu/3600.jpg", new Guid("eb5f667a-201a-4727-a367-edc3f914b32d") },
                    { new Guid("81d5be13-166d-49ba-a8b8-0e308fd175d0"), "assets/images/products/vga/550m2.jpg", new Guid("380825b1-2650-4b48-8cc2-d9c35a509ba8") },
                    { new Guid("854c96c6-9981-4484-90b1-96842521fb47"), "assets/images/products/mouse/a918black.jpg", new Guid("5b3d98f9-efa7-4503-a9b1-fbde14fc94de") },
                    { new Guid("855fd201-468c-4e01-82dc-f8e2c30845a2"), "assets/images/products/vga/570m2.jpg", new Guid("1db6c932-e9ee-4ffd-bbf0-92ef23061982") },
                    { new Guid("9c7609cd-efa7-4a58-ad29-821e4198da99"), "assets/images/products/main/x570tuf.jpg", new Guid("a180a819-51e9-46c9-b4e0-6e406a6fc658") },
                    { new Guid("9e4de8be-da82-4cf4-85a5-3f07c1450df1"), "assets/images/products/keyboard/dareuek810.jpg", new Guid("9cbcc2e0-0dbf-437f-92c7-c18e96852a2d") },
                    { new Guid("ac26640d-cc18-4b92-bb43-13450d04538f"), "assets/images/products/cpu/3100.jpg", new Guid("88142b2b-d710-46c1-87b0-ca4fb9e8ee6e") },
                    { new Guid("b8a4cd40-97ee-48ca-a1aa-014b1f3c6301"), "assets/images/products/monitor/edra24.jpg", new Guid("401da5d4-6f94-44ea-a4c7-3489db8056e8") },
                    { new Guid("e64a72d9-cb1a-430c-9508-dbc8ac06170f"), "assets/images/products/main/x570ace.jpg", new Guid("33ed75de-1f7f-41aa-bb3d-fe2ab5f9d265") },
                    { new Guid("e81443a7-7299-435c-8b8b-abe7459d6647"), "assets/images/products/mouse/a960y.jpg", new Guid("dd8d66e3-3d59-415c-8e82-e727b3e215bd") },
                    { new Guid("ecd00c3e-5839-4515-822e-740458bf5e0d"), "assets/images/products/vga/550m1.jpg", new Guid("380825b1-2650-4b48-8cc2-d9c35a509ba8") },
                    { new Guid("ed442459-948c-486b-86f5-84479c4ce8df"), "assets/images/products/cpu/3600.jpg", new Guid("8361d4af-7be2-4f20-b544-d54c1d149f90") },
                    { new Guid("f15a682a-0bd1-4857-be4c-f5b1fc623c35"), "assets/images/products/vga/570m1.jpg", new Guid("1db6c932-e9ee-4ffd-bbf0-92ef23061982") },
                    { new Guid("f1e25cc4-6375-4a41-b21e-bd7428a8cba1"), "assets/images/products/main/a520m1.jpg", new Guid("33005ad9-ddd8-4dbb-b025-669d781f4b5f") },
                    { new Guid("f4cc692a-4492-48ac-8b87-3680096c4f63"), "assets/images/products/vga/rx6600xt1.jpg", new Guid("1fc51323-7607-4a7f-aaf7-05f09b0b3dfb") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductImages_ProductId",
                table: "ProductImages",
                column: "ProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_AuthorId",
                table: "Products",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Products_CategoryId",
                table: "Products",
                column: "CategoryId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductImages");

            migrationBuilder.DropTable(
                name: "Products");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
