﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using ProductAPI.Data;
using ProductAPI.Models;
using ProductAPI.Models.Dto;
using ProductAPI.Services.IService;

namespace ProductAPI.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductAPIController : ControllerBase
    {
        private readonly AppDbContext _db;
        private ResponseDto _response;
        private IMapper _mapper;
        private readonly IMessage _message;
        private readonly IGetService _getService;
        private readonly IProductService _productService;

        public ProductAPIController(AppDbContext db, IMapper mapper, IMessage message, IGetService getService,  IProductService productService)
        {
            _db = db;
            _mapper = mapper;
            _response = new ResponseDto();
            _message = message;
            _getService = getService;
            _productService = productService;

        }

        [HttpGet]
        public async Task<IActionResult> GetProduct(int page, string? searchField)
        {
            ResponseDto res = await _getService.GetProduct(page, searchField);
           
            return Ok(res);
        }

        [HttpGet]
        [Route("GetAllProduct")]
        public async Task<IActionResult> GetAllProduct()
        {
            ResponseDto res = await _getService.GetAllProduct();
           
            return Ok(res);
        }

        [HttpGet]
        [Route("{id:guid}")]
        public async Task<IActionResult> GetByIdProduct(Guid id)
        {
            ResponseDto res = await _getService.GetByIdProduct(id);
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetRecommend")]
        public async Task<IActionResult> GetRecommend()
        {
            ResponseDto res = await _getService.GetRecommend();
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetBestBuy")]
        public async Task<IActionResult> GetBestBuy()
        {
            ResponseDto res = await _getService.GetBestBuy();
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetRelate/{id:guid}")]
        public async Task<IActionResult> GetRelate(Guid id)
        {
            ResponseDto res = await _getService.GetRelate(id);
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetCategory")]
        public async Task<IActionResult> GetCategory(int page, string? searchField)
        {
            ResponseDto res = await _getService.GetCategory(page, searchField);
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetAuthor")]
        public async Task<IActionResult> GetAuthor(int page, string? searchField)
        {
            ResponseDto res = await _getService.GetAuthor(page, searchField);
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetAllCategory")]
        public async Task<IActionResult> GetAllCategory()
        {
            ResponseDto res = await _getService.GetAllCategory();
            
            return Ok(res);
        }

        [HttpGet]
        [Route("GetAllAuthor")]
        public async Task<IActionResult> GetAllAuthor()
        {
            ResponseDto res = await _getService.GetAllAuthor();
            
            return Ok(res);
        }



        [HttpGet]
        [Route("GetProductByCategory/{id}")]
        public async Task<IActionResult> GetProductByCategory(int id)
        {
            ResponseDto res = await _getService.GetProductByCategory(id);
           
            return Ok(res);
        }

        [HttpGet]
        [Route("GetProductByAuthor/{id}")]
        public async Task<IActionResult> GetProductByAuthor(int id)
        {
            ResponseDto res = await _getService.GetProductByAuthor(id);
           
            return Ok(res);
        }

        [HttpGet]
        [Route("GetProductByAuthorCategory")]
        public async Task<IActionResult> GetProductByAuthorCategory(int? idAuthor, int? idCategory, int? minPrice, int? maxPrice)
        {
            ResponseDto res = await _getService.GetProductByAuthorCategory(idAuthor,idCategory,minPrice,maxPrice);
           
            return Ok(res);
        }


        [HttpGet("GetByIdCategory/{id:int}")]
        public async Task<IActionResult> GetByIdCategory(int id)
        {
            ResponseDto res = await _getService.GetByIdCategory(id);
           
            return Ok(res);
        }

        [HttpGet("GetByIdAuthor/{id:int}")]
        public async Task<IActionResult> GetByIdAuthor(int id)
        {
            ResponseDto res = await _getService.GetByIdAuthor(id);
           
            return Ok(res);
        }


        [Consumes("multipart/form-data")]
        [HttpPost("UpsertCategory")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpsertCategory([FromForm] IFormFile? imageFile, [FromForm] string categoryJson)
        {

            ResponseDto res = await _productService.UpsertCategory(imageFile, categoryJson);
           
            return Ok(res);
        }

        [Consumes("multipart/form-data")]
        [HttpPost("UpsertProduct")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpsertProduct([FromForm] List<IFormFile>? imageFiles, [FromForm] string productDtoJson)
        {
            ResponseDto res = await _productService.UpsertProduct(imageFiles, productDtoJson);
          
            return Ok(res);

        }
        
        [HttpPost("UpsertAuthor")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpsertAuthor(Author author)
        {

            ResponseDto res = await _productService.UpsertAuthor(author);
            
            return Ok(res);
        }

        [HttpDelete("DeleteCategory")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteCategory(int id)
        {
            ResponseDto res = await _productService.DeleteCategory(id);
            
            return Ok(res);

        }

        [HttpDelete("DeleteAuthor")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteAuthor(int id)
        {
            ResponseDto res = await _productService.DeleteAuthor(id);
           
            return Ok(res);

        }


        [HttpDelete("DeleteProduct/{productId}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteProduct(Guid productId)
        {

            ResponseDto res = await _productService.DeleteProduct(productId);
           
            return Ok(res);
        }



        [HttpDelete]
        [Route("DeleteProductImage/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteProductImage(Guid id)
        {

            ResponseDto res = await _productService.DeleteProductImage(id);
           
            return Ok(res);
        }
    }
}
