﻿namespace ProductAPI.Models.Dto
{
    public class ProductVM
    {

        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public int? BuyCount { get; set; }
        public double ListPrice { get; set; }

        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        
        public string? AuthorName { get; set; }

        public string? CategoryName { get; set; }

        public int? AuthorId { get; set; }

        public int? CategoryId { get; set; }

        public List<ProductImageDto> ProductImages { get; set; }
    }
}
