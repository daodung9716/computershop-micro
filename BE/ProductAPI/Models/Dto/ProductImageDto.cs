﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ProductAPI.Models.Dto
{
    public class ProductImageDto
    {
        public Guid? Id { get; set; }
        [Required]
        public string? ImageUrl { get; set; }
        public Guid? ProductId { get; set; }

    }
}
