﻿namespace ProductAPI.Models.Dto
{
    public class ResponseDto
    {
        public ResponseDto()
        {
            RecordsTotal = 0;          
        }
        public object? Result { get; set; }
        public bool IsSuccess { get; set; } = true;
        public string Message { get; set; } = "";

        public int PageSize { get; set; }

        public int TotalPages { get; set; }
       

        public int RecordsTotal { get; set; }

        public int RecordPerPage { get; set; }
    }
}
