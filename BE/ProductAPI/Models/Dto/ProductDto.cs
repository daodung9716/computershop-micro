﻿namespace ProductAPI.Models.Dto
{
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public double? Price { get; set; }

        public int? BuyCount { get; set; }
        public double? ListPrice { get; set; }
        public string? Description { get; set; }

        public int? AuthorId { get; set; }

        public int? CategoryId { get; set; }
      

        public string? ImageUrl { get; set; }

        public List<ProductImageDto>? ProductImages { get; set; }
       
    }
}
