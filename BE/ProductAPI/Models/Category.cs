﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ProductAPI.Models
{
    public class Category
    {
        [Key]
        public int Id { get; set; }

        [Required]       
        public string Name { get; set; }

        public string?  Image { get; set; }

        public string? ImageLocalPath { get; set; }

        public int? DisplayOrder { get; set; }
        //DisplayOrder trong trường hợp này là một thuộc tính số nguyên được sử dụng để xác định thứ tự
        //hiển thị hoặc sắp xếp của một danh mục hoặc một mục trong ứng dụng của bạn.
    }
}
