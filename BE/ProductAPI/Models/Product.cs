﻿
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductAPI.Models
{
    public class Product
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Range(1, 1000)]
        public double Price { get; set; }

        public double ListPrice { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
       

        public int? BuyCount { get; set; }
     
        public int AuthorId { get; set; }


        [ForeignKey("AuthorId")]
        [ValidateNever]
        public Author Author { get; set; }

   
        public int CategoryId { get; set; }


        [ForeignKey("CategoryId")]
        [ValidateNever]
        public Category Category { get; set; }

        [ValidateNever]
        public List<ProductImage> ProductImages { get; set; }

    }
}
