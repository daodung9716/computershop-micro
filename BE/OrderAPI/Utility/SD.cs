﻿namespace OrderAPI.Utility
{
    public class SD
    {
        public const string Status_Pending = "Pending";
        public const string Status_Paid = "Paid";
        public const string Status_Shipping = "Shipping";
        public const string Status_Received = "Received";     
        public const string Status_Refunded = "Refunded";
        public const string Status_Cancelled = "Cancelled";
        public const string Status_Completed = "Completed";

        public const string RoleAdmin = "Admin";
        public const string RoleCustomer = "Customer";
    }
}
