﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Stripe.Checkout;
using Stripe;
using Microsoft.EntityFrameworkCore;
using OrderAPI.Models.Dto;
using OrderAPI.Data;
using OrderAPI.Service.IService;
using OrderAPI.Models;
using OrderAPI.Utility;
using System.Security.Claims;

namespace OrderAPI.Controllers
{
    [Route("api/order")]
    [ApiController]
    public class OrderAPIController : ControllerBase
    {

        private readonly IOrderService _orderService;

        public OrderAPIController( IOrderService orderService)
        {
        
            _orderService = orderService;

        }

        [Authorize]
        [HttpGet("GetAllOrders")]
        public async Task<IActionResult> GetAllOrders(string? userId = "")
        {
            ResponseDto res = new ResponseDto();
            if (User.IsInRole("Admin"))
            {
                res = await _orderService.GetAllOrders();
            }
            else
            {
                res = await _orderService.GetAllOrders(userId);
            }

            if (res.IsSuccess == false)
            {
                return BadRequest(res);
            }
            return Ok(res);
        }

        [Authorize]
        [HttpGet("GetOrder/{id:int}")]
        public async Task<IActionResult> GetOrder(int id)
        {
            ResponseDto res = await _orderService.GetOrder(id);
           
            return Ok(res);
        }


        [Authorize]
        [HttpPost("CreateOrder")]
        public async Task<IActionResult> CreateOrder( CartDto cartDto)
        {

            var claimsIdentity = (ClaimsIdentity)User.Identity;
            var userId = claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value;
            cartDto.CartHeader.UserId = userId;

            ResponseDto res = await _orderService.CreateOrder(cartDto);
           
            return Ok(res);
        }


        [Authorize]
        [HttpPost("UpdateOrder")]
        public async Task<IActionResult> UpdateOrder(OrderHeaderUpdate dto)
        {
        
            ResponseDto res = await _orderService.UpdateOrder(dto);
           
            return Ok(res);
        }



        [Authorize]
        [HttpPost("CreateStripeSession")]
        public async Task<IActionResult> CreateStripeSession([FromBody] StripeRequestDto stripeRequestDto)
        {
            ResponseDto res = await _orderService.CreateStripeSession(stripeRequestDto);
            
            return Ok(res);
        }


        [Authorize]
        [HttpPost("ValidateStripeSession")]
        public async Task<IActionResult> ValidateStripeSession([FromBody] int orderHeaderId)
        {
            ResponseDto res = await _orderService.ValidateStripeSession(orderHeaderId);
           
            return Ok(res);
        }


        [Authorize]
        [HttpPost("UpdateOrderStatus/{orderId:int}")]
        public async Task<IActionResult> UpdateOrderStatus(int orderId, [FromBody] string newStatus)
        {
            ResponseDto res = await _orderService.UpdateOrderStatus(orderId,newStatus);
           
            return Ok(res);
        }

       
    }
}
