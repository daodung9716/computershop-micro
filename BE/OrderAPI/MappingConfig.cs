﻿using AutoMapper;
using OrderAPI.Models;
using OrderAPI.Models.Dto;

namespace OrderAPI
{
    public class MappingConfig
    {
        public static MapperConfiguration RegisterMaps()
        {
            var mappingConfig = new MapperConfiguration(config =>
            {
                config.CreateMap<OrderHeaderDto, CartHeaderDto>()
                .ForMember(dest=>dest.CartTotal, u=>u.MapFrom(src=>src.OrderTotal)).ReverseMap();

                config.CreateMap<CartDetailsDto, OrderDetailsDto>()
                .ForMember(dest => dest.ProductName, u => u.MapFrom(src => src.Product.Name))
                .ForMember(dest => dest.Price, u => u.MapFrom(src => src.Product.Price));

                config.CreateMap<OrderDetailsDto, CartDetailsDto>();

                config.CreateMap<OrderHeader, OrderHeaderDto>()
             .ForMember(dest => dest.PaymentIntentId, opt => opt.Ignore())
             .ForMember(dest => dest.StripeSessionId, opt => opt.Ignore())
             .ReverseMap();
            //    .ForMember(dest => dest.DestName, opt => opt.MapFrom(src => src.SourceName))
            //.ForMember(dest => dest.DestAge, opt => opt.MapFrom(src => src.SourceAge));
                config.CreateMap<OrderDetailsDto, OrderDetails>().ReverseMap();

            });
            return mappingConfig;
        }
    }
}
