﻿using Microsoft.AspNetCore.Mvc;
using OrderAPI.Models.Dto;

namespace OrderAPI.Service.IService
{
    public interface IOrderService
    {

        Task<ResponseDto> GetAllOrders();

        Task<ResponseDto> GetAllOrders(string userId);

        Task<ResponseDto> GetOrder(int id);

        Task<ResponseDto> CreateOrder(CartDto cartDto);

        Task<ResponseDto> UpdateOrder(OrderHeaderUpdate dto);

        Task<ResponseDto> CreateStripeSession( StripeRequestDto stripeRequestDto);
       


        Task<ResponseDto> ValidateStripeSession(int orderHeaderId);

        Task<ResponseDto> UpdateOrderStatus(int orderId,  string newStatus);
    }
}
