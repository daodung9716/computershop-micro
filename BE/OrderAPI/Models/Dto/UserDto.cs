﻿namespace OrderAPI.Models.Dto
{
    public class UserDto
    {

        public string Name { get; set; }

        public string Address { get; set; }

        public string Company { get; set; }

        public string Email { get; set; }
    }
}
