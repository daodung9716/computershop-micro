﻿namespace OrderAPI.Models.Dto
{
    public class OrderHeaderUpdate
    {
        public int OrderHeaderId { get; set; }
      
        
        public string? Name { get; set; }

        public string? Phone { get; set; }

        public string? Email { get; set; }

        public string? Address { get; set; }
       
    }
}
