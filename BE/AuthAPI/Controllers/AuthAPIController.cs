﻿
using AuthAPI.Data;
using AuthAPI.Models;
using AuthAPI.Models.Dto;
using AuthAPI.Service.IService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace AuthAPI.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthAPIController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IUserService _userService;
        public AuthAPIController(IAuthService authService, AppDbContext db, IUserService userService)
        {
            _authService = authService;
            _userService = userService;

        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] UserDto model)
        {
            ResponseDto res = await _authService.Register(model);
           
            return Ok(res);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginRequestDto model)
        {
            ResponseDto loginResponse = await _authService.Login(model);
            

            return Ok(loginResponse);

        }

        [HttpPost]
        [Route("refresh")]
        public async Task<IActionResult> Refresh(TokenApi model)
        {
            var res = new ResponseDto();
            var refresh = await _authService.Refresh(model);
            if (refresh == null)
            {
                res.IsSuccess = false;
                res.Message = "Can't Refresh Token";
                return BadRequest(res);
            }
            res.Result = refresh;

            return Ok(res);
        }

        [HttpPost("assignRole")]
        public async Task<IActionResult> AssignRole(string email, string roleName)
        {

            ResponseDto res = await _authService.AssignRole(email, roleName);
            

            return Ok(res);

        }



        [HttpGet]
        [Route("checkEmailExist/{email}")]
        [AllowAnonymous]
        public async Task<IActionResult> CheckEmailExist(string email)
        {

            ResponseDto result = _authService.CheckEmail(email);
        
            return Ok(result);
        }


        [HttpGet("GetUsers")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetUsers(int page, string? searchField)
        {
            ResponseDto result = await _userService.GetUsers(page, searchField);
            
            return Ok(result);
        }


        [HttpGet("GetByIdUser/{userId}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetByIdUser(string userId)
        {
            ResponseDto result = await _userService.GetByIdUser(userId);
            
            return Ok(result);
        }

        [HttpPost("UpdateUser")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateUser(UserDto dto)
        {

            ResponseDto result = await _userService.UpdateUser(dto);
            
            return Ok(result);

        }


        [HttpDelete("DeleteUser/{userId}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(string userId)
        {

            ResponseDto result = await _userService.Delete(userId);
           
            return Ok(result);
        }


    }
}
