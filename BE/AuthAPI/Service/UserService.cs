﻿using AuthAPI.Data;
using AuthAPI.Models;
using AuthAPI.Models.Dto;
using AuthAPI.Service.IService;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace AuthAPI.Service
{
    public class UserService : IUserService
    {
        private readonly AppDbContext _db;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserService(AppDbContext db, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _roleManager = roleManager;
            _userManager = userManager;
        }

        public async Task<ResponseDto> Delete(string userId)
        {
            var res = new ResponseDto();
            try
            {
                var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == userId);

                if (user == null)
                {
                    res.IsSuccess = false;
                    res.Message = $"No users existed in the  system!";
                    return res;
                }
                var rolesToExclude = new List<string> { "Admin" };


                // không thể xóa Vip,Admin
                foreach (var role in rolesToExclude)
                {
                    if (await _userManager.IsInRoleAsync(user, role))
                    {
                        res.IsSuccess = false;
                        res.Message = $"You do not have permission to delete this user.";
                        return res;
                    }
                }

                _db.Users.Remove(user);

                await _db.SaveChangesAsync();
                res.Message = $"Delete *{user.Name}*  Successful!";
            }
            catch (Exception ex)
            {

                res.IsSuccess = false;
                res.Message = "Error  delete users!";
                return res;

            }
            return res;
        }

        public async Task<ResponseDto> GetByIdUser(string userId)
        {
            var result = new ResponseDto();
            try
            {
                var user = _db.Users.FirstOrDefault(x => x.Id == userId);
                if (user == null)
                {
                    result.IsSuccess = false;
                    result.Message = "No users existed in the  system";
                    return result;
                }

                var query = await (from users in _db.Users.AsNoTracking()
                                   join r in _db.UserRoles on users.Id equals r.UserId into userRoles
                                   from urs in userRoles.DefaultIfEmpty()
                                   join r in _db.Roles on urs.RoleId equals r.Id into roles
                                   from rr in roles.DefaultIfEmpty()
                                   where users.Id == userId
                                   select new UserDto
                                   {
                                       Id=users.Id,
                                       Email = users.Email,
                                       Name = users.Name,
                                       PhoneNumber = users.PhoneNumber,
                                       Address = users.Address,
                                       Role = rr.Name

                                   }).ToListAsync();


                result.Result = query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                return result;
            }
            return result;
        }

        public async Task<ResponseDto> GetUsers(int page, string? searchField)
        {
            var result = new ResponseDto();
            int pageSize = 10;
            try
            {
                var query = await (from users in _db.Users.AsNoTracking()
                                   join r in _db.UserRoles on users.Id equals r.UserId into userRoles
                                   from urs in userRoles.DefaultIfEmpty()
                                   join r in _db.Roles on urs.RoleId equals r.Id into roles
                                   from rr in roles.DefaultIfEmpty()
                                   where (string.IsNullOrEmpty(searchField)) || users.Name.ToLower().Contains(searchField.ToLower())

                                   select new UserDto
                                   {
                                       Id = users.Id,
                                       Email = users.Email,
                                       Name = users.Name,
                                       PhoneNumber = users.PhoneNumber,
                                       Address = users.Address,
                                       Role = rr.Name

                                   }).ToListAsync();

                result.PageSize = pageSize;
                result.TotalPages = (int)Math.Ceiling((double)query.Count() / pageSize);
                var number = query.Skip((page - 1) * pageSize).Take(pageSize).ToList();
                result.Result = number;
                result.RecordPerPage = number.Count();
                result.RecordsTotal = query.Count();


            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public async Task<ResponseDto> UpdateUser(UserDto dto)
        {
            var result = new ResponseDto();
            try
            {
                var user = await _db.ApplicationUsers.FirstOrDefaultAsync(x => x.Id == dto.Id);
                if (user == null)
                {
                    result.Message = $"No users with {dto.Name} in the existing system!";
                    result.IsSuccess = false;
                    return result;
                }

                //không thể thay đổi EMAIL
                user.Address = dto.Address;
                user.Name = dto.Name;
                user.PhoneNumber = dto.PhoneNumber;
                //UPDATE ROLE
                if (!string.IsNullOrEmpty(dto.Role))
                {
                    var role = await _roleManager.FindByIdAsync(dto.Role);
                    if (role != null)//roll tồn tại
                    {
                        // Kiểm tra xem người dùng có thuộc role đó hay không                   
                        if (!await _userManager.IsInRoleAsync(user, role.Name))
                        {
                            var currentRoles = await _userManager.GetRolesAsync(user);
                            await _userManager.RemoveFromRolesAsync(user, currentRoles);
                            await _userManager.AddToRoleAsync(user, role.Name);
                            await _userManager.UpdateAsync(user);
                        }
                    }
                }

                await _db.SaveChangesAsync();
                result.Message = "Update User Successfully";
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
