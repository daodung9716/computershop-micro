﻿using AuthAPI.Data;
using AuthAPI.Models;
using AuthAPI.Models.Dto;
using AuthAPI.Service.IService;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace AuthAPI.Service
{
    public class AuthService : IAuthService
    {
        private readonly AppDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;
        private readonly IConfiguration _configuration;
        private readonly JwtOptions _jwtOptions;

        public AuthService(AppDbContext db, IJwtTokenGenerator jwtTokenGenerator,
            UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration, IOptions<JwtOptions> jwtOptions)
        {
            _db = db;
            _jwtTokenGenerator = jwtTokenGenerator;
            _userManager = userManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _jwtOptions = jwtOptions.Value;
        }

        //gán vai trò cho người dùng
        public async Task<ResponseDto> AssignRole(string email, string roleName)
        {
            var res = new ResponseDto();
            var user = _db.ApplicationUsers.FirstOrDefault(u => u.Email.ToLower() == email.ToLower());
            var role =_db.Roles.FirstOrDefault(x =>x.Name.ToLower() == roleName.ToLower());
            if(role == null)
            {
                res.IsSuccess = false;
                res.Message = "No role in system";
                return res;
            }
            if (user != null)
            {
                if (!_roleManager.RoleExistsAsync(roleName).GetAwaiter().GetResult())
                {
                    //create role if it does not exist
                    _roleManager.CreateAsync(new IdentityRole(roleName)).GetAwaiter().GetResult();
                }
                await _userManager.AddToRoleAsync(user, roleName);
             
                res.Message = "Add role to user successful";
            }
            else
            {
                res.IsSuccess = false;
                res.Message = "No user in system";
                return res;
            }
            return res;

        }

        public async Task<ResponseDto> Login(LoginRequestDto loginRequestDto)
        {
            var res = new ResponseDto();

            var user = _db.ApplicationUsers.FirstOrDefault(u => u.UserName.ToLower() == loginRequestDto.UserName.ToLower());

            bool isValid = await _userManager.CheckPasswordAsync(user, loginRequestDto.Password);

            if (user == null || isValid == false)
            {
                res.IsSuccess = false;
                res.Message = "UserName or Password doesn't exactly";
                return res;
            }

            //if user was found , Generate JWT Token
            var roles = await _userManager.GetRolesAsync(user);
            var token = _jwtTokenGenerator.GenerateToken(user, roles);
            var refreshToken = CreateRefreshToken();
            user.RefreshToken = refreshToken;
            user.RefreshTokenExpiryTime = DateTime.Now.AddHours(5);
            await _db.SaveChangesAsync();
           
            LoginResponseDto loginResponseDto = new LoginResponseDto()
            {             
                Token = token,
                RefreshToken = refreshToken,
            };


            res.Result = loginResponseDto;
            res.Message = "Login successful";

            return res ;
        }


        public async Task<ResponseDto> Register(UserDto dto)
        {
            var res = new ResponseDto();
            ApplicationUser user = new()
            {
                UserName = dto.Email,
                Address = dto.Address,
                Email = dto.Email,
                NormalizedEmail = dto.Email.ToUpper(),
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber
            };

            try
            {
                var result = await _userManager.CreateAsync(user, dto.Password);
                if (result.Succeeded)
                {
                    // Lấy thông tin người dùng đã được tạo
                    var userToReturn = _db.ApplicationUsers.First(u => u.UserName == dto.Email);

                    // Tạo Role nếu nó chưa tồn tại
                    if (!await _roleManager.RoleExistsAsync(dto.Role))
                    {
                        var role = new IdentityRole
                        {
                            Name = dto.Role
                        };
                        await _roleManager.CreateAsync(role);
                    }

                    // Gán Role cho người dùng
                    await _userManager.AddToRoleAsync(userToReturn, dto.Role);

                    // Tạo DTO cho người dùng để trả về
                    UserDto userDto = new()
                    {
                        Email = userToReturn.Email,
                        Id = userToReturn.Id,
                        Name = userToReturn.Name,
                        PhoneNumber = userToReturn.PhoneNumber
                    };

                    res.Result = userDto;
                    res.Message = "User registered successfully";

                }
                else
                {
                    res.IsSuccess = false;
                    res.Message = "User registered failed";
                    return res;
                }
            }
            catch (Exception ex)
            {
                res.IsSuccess = false;
                res.Message = ex.Message;
                return res;
            }
            return res;
        }

        public async Task<ResponseDto> Refresh(TokenApi model)
        {
            var res = new ResponseDto();
            string accessToken = null;
            string refreshToken = null;
         
            if (model != null)
            {
                accessToken = model.AccessToken;
                refreshToken = model.RefreshToken;
            }
            else
            {
                res.IsSuccess = false;
                res.Message = "No access token";
                return res;
            }

            //Xác định quyền truy cập mà người dùng đã có khi token còn hiệu lực.
            var principal = GetPrincipalFromExpiredToken(accessToken);

            if (principal is null)//thêm phần này
            {
                res.IsSuccess = false;
                res.Message = "Can't principal from expired token";
                return res;

            }
            //thêm phần này,giải mã username từ TOKEN
            var username = principal.Claims.FirstOrDefault(c => c.Type == "name")?.Value; //cách lấy default
            if (username == null)
            {
                res.IsSuccess = false;
                res.Message = "Can't get username from principal ";
                return res;
            }
            var user = await _db.Users.FirstOrDefaultAsync(u => u.UserName == username);
            if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
            { // viết sai  user != null ở đây
                res.IsSuccess = false;
                res.Message = "Not user in system from  principal";
                return res;
            }
            var roles = await _userManager.GetRolesAsync(user);
            var tokenCreate = _jwtTokenGenerator.GenerateToken(user, roles);
            user.Token = tokenCreate;
            var newAccessToken = user.Token;
            var newRefreshToken = CreateRefreshToken();
            user.RefreshToken = newRefreshToken;

            await _db.SaveChangesAsync();

            LoginResponseDto loginResponseDto = new LoginResponseDto()
            {

                Token = newAccessToken,
                RefreshToken = newRefreshToken,
            };

            res.Result= loginResponseDto;
            res.Message = "Generate Refresh Token Successful";
            return res;

        }


        private string CreateRefreshToken()
        {
            var tokenBytes = RandomNumberGenerator.GetBytes(64);
            var refreshToken = Convert.ToBase64String(tokenBytes);
            var tokenInUser = _db.Users.Any(
                a => a.RefreshToken == refreshToken);
            if (tokenInUser)
            {
                return CreateRefreshToken();
            }
            return refreshToken;
        }


        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var key = Encoding.ASCII.GetBytes(_jwtOptions.Key);
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidAudience = _jwtOptions.Audience,
                ValidIssuer = _jwtOptions.Issuer,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);

            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals("HS512", StringComparison.InvariantCultureIgnoreCase))
            {
                throw new SecurityTokenException("Invalid token");
            }


            return principal;
        }

        public ResponseDto CheckEmail(string email)
        {
            var res = new ResponseDto();

            if (!Regex.IsMatch(email, @"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"))
            {
                res.IsSuccess = false;
                res.Message = "Email Not Valid";
                return res;
            }

            var list = _db.Users.AsNoTracking().ToList().Select(x => x.Email);
            var result = list.FirstOrDefault(x => x.Equals(email));
            if (result != null)
            {
                res.IsSuccess = false;
                res.Message = "Email already existed in system";
                return res;

            }

            //  true nghĩa là email chưa được sử dụng.
            return res;

        }





    }
}
