﻿using AuthAPI.Models.Dto;

namespace AuthAPI.Service.IService
{
    public interface IAuthService
    {
        Task<ResponseDto> Register(UserDto dto);
        Task<ResponseDto> Login(LoginRequestDto loginRequestDto);
        Task<ResponseDto> AssignRole(string email, string roleName);

        Task<ResponseDto> Refresh(TokenApi model);

        ResponseDto CheckEmail(string email);

      
    }
}
