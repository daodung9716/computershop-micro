﻿using AuthAPI.Models.Dto;
using Microsoft.AspNetCore.Mvc;

namespace AuthAPI.Service.IService
{
    public interface IUserService
    {
        Task<ResponseDto> GetUsers(int page, string? searchField);

        Task<ResponseDto> GetByIdUser(string userId);

        Task<ResponseDto> UpdateUser(UserDto dto);


        Task<ResponseDto> Delete(string userId);

    }
}
