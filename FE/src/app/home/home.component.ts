import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../services/product/product.service';
import { Category } from '../models/category';
import {  ProductVM } from '../models/product';
import { LoadingService } from '../services/loading.service';
import { CartService } from '../services/cart/cart.service';
import { CartDto } from '../models/cart';
import { ResponseDto } from '../models/responsedto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  categories: Category[] = [];
  products: ProductVM[] = [];
  productBest: ProductVM[] = [];


  constructor(
    private productSer: ProductService, private cartService: CartService,  private toastr: ToastrService,
    private router: Router, private loadingService: LoadingService
  ) { }


  ngOnInit(): void {
    this.productSer.GetCategory(1,'').subscribe(response => {
      this.loadingService.startLoading();
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.categories = response.result;
      } else {
        console.error('Failed to fetch categories:', response.message);
      }
    });

    this.productSer.GetRecommend().subscribe(response => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.products = response.result;
      } else {
        // Xử lý khi yêu cầu không thành công
        console.error('Failed to fetch product Recommend');
      }
    });

    this.productSer.GetBestBuy().subscribe(response => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.productBest = response.result;
      } else {
        // Xử lý khi yêu cầu không thành công
        console.error('Failed to fetch product Best');
      }
    });
  }

  CartUpsert(product: ProductVM) {
    const cartDto: CartDto = {
      cartHeader:{},
      cartDetails: [{
        productId:product.id,
        count: 1,
      }]
    }
    this.cartService.CartUpsert(cartDto).subscribe( {
      next: (res:ResponseDto<CartDto>) => {
        if (res.isSuccess == false) {
          this.toastr.error(` ${res.message}`, `Failed`, {
            timeOut: 5000,
          });
        } else {
          this.toastr.success(` ${res.message}`, `Success`, {
            timeOut: 5000,
          });
        }
          this.router.navigate(['/home']);
        }, error: (err: any) => {
          this.toastr.error(` ${err.message}`, `Failed`, {
            timeOut: 5000,
          });
        }
      })
    }


  features = [
    { id: 1, name: 'Free Shipping', text: 'Order over $200', image: 'assets/images/icons/delivery-van.svg' },
    { id: 2, name: 'Money Returns', text: '30 days money returns', image: 'assets/images/icons/money-back.svg' },
    { id: 3, name: '24/7 Support', text: 'Customer support', image: 'assets/images/icons/service-hours.svg' },
  ];



}
