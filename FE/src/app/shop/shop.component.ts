import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductService } from '../services/product/product.service';
import { ProductVM } from '../models/product';
import { Category } from '../models/category';
import { Author } from '../models/author';
import { CartDto } from '../models/cart';
import { CartService } from '../services/cart/cart.service';
import { ResponseDto } from '../models/responsedto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
})
export class ShopComponent implements OnInit {
  products: ProductVM[] = [];
  categories: Category[] = [];
  brands: Author[] = [];
  page: number = 1;
  pageSize!: number;
  totalPages!: number;
  selectedCategoryId: string | null = null;
  selectedAuthorId: string | null = null;
  minPrice: number | null = null;
  maxPrice: number | null = null;
  selectedSortOption: string = '';
  searchField: string = '';

  constructor(
    private route: ActivatedRoute, private cartService: CartService,  private toastr: ToastrService,
    private productSer: ProductService, private router: Router
  ) { }

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      this.searchField = params.get('searchField')!;
      this.getProduct();
    });

    this.route.paramMap.subscribe(params => {
      this.selectedCategoryId = params.get('id')!;
      this.getProductbyAuthorCategory();
    });

    this.productSer.GetAllCategory().subscribe((response) => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.categories = response.result;
      } else {
        console.error('Failed to fetch categories:', response.message);
      }
    });

    this.productSer.GetAllAuthor().subscribe((response) => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.brands = response.result;
      } else {
        console.error('Failed to fetch categories:', response.message);
      }
    });


  }

  onCategoryCheckboxChange(id: string): void {
    if (this.selectedCategoryId === id) {
      // Đã chọn checkbox đang được chọn, bỏ chọn nó.
      this.selectedCategoryId = null;
    } else {
      // Chọn checkbox mới và bỏ chọn checkbox cũ (nếu có).
      this.selectedCategoryId = id;
    }

    this.getProductbyAuthorCategory();
  }

  onAuthorCheckboxChange(id: string): void {
    if (this.selectedAuthorId === id) {
      this.selectedAuthorId = null;
    } else {
      this.selectedAuthorId = id;
    }
    this.getProductbyAuthorCategory();
  }

  getProductbyAuthorCategory(): void {
    this.productSer
      .GetProductByAuthorCategory(
        this.selectedAuthorId, this.selectedCategoryId, this.minPrice, this.maxPrice)
      .subscribe((response) => {
        if (response.isSuccess) {
          this.products = response.result;
          this.totalPages = response.totalPages;
          if (this.selectedSortOption === 'price-low-to-high') {
            this.products.sort((a, b) => a.price - b.price);
          }

          if (this.selectedSortOption === 'price-high-to-low') {
            this.products.sort((a, b) => b.price - a.price);
          }

        } else {
          console.error('Failed get product by author category', response.message);
        }
      });
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getProduct(): void {

    this.productSer
      .GetProduct(
        this.page, this.searchField)
      .subscribe((response) => {
        if (response.isSuccess) {
          this.products = response.result;
          this.totalPages = response.totalPages;
          if (this.selectedSortOption === 'price-low-to-high') {
            this.products.sort((a, b) => a.price - b.price);
          }

          if (this.selectedSortOption === 'price-high-to-low') {
            this.products.sort((a, b) => b.price - a.price);
          }

        } else {
          console.error('Failed get product by search', response.message);
        }
      });
    window.scrollTo({ top: 0, behavior: 'smooth' });

  }

  getPagesArray() {
    return Array.from({ length: this.totalPages }, (_, index) => index + 1);
  }

  changePage(page: number) {
    if (page < 1 || page > this.totalPages) return;
    this.page = page;
    this.getProduct();
  }

  CartUpsert(product: ProductVM) {
    const cartDto: CartDto = {
      cartHeader: {},
      cartDetails: [{
        productId: product.id,
        count: 1,
      }]
    }
    this.cartService.CartUpsert(cartDto).subscribe({
      next: (res: ResponseDto<CartDto>) => {
        if (res.isSuccess == false) {
          this.toastr.error(` ${res.message}`, `Failed`, {
            timeOut: 5000,
          });
        } else {
          this.toastr.success(` ${res.message}`, `Success`, {
            timeOut: 5000,
          });
        }
      }, error: (err: any) => {
        this.toastr.error(` ${err.message}`, `Failed`, {
          timeOut: 5000,
        });
      }

    })

  }



  sortProducts(): void {
    if (this.selectedSortOption === 'price-low-to-high') {
      // Sắp xếp sản phẩm theo giá từ thấp đến cao
      this.products.sort((a, b) => a.price - b.price);
    } else if (this.selectedSortOption === 'price-high-to-low') {
      // Sắp xếp sản phẩm theo giá từ cao đến thấp
      this.products.sort((a, b) => b.price - a.price);
    } else {
    }
  }
}
