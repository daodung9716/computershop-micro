import { Component, OnInit } from '@angular/core';
import { LoginModel, LoginResponse } from '../models/login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../services/auth/auth.service';
import { Router } from '@angular/router';
import { StoreTokenService } from '../services/auth/storetoken.service';
import { ResponseDto } from '../models/responsedto';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent  implements OnInit {
  logInModel!: LoginModel;

  type: string = 'password';
  eyeIcon: string = 'fa-eye-slash';
  isText: boolean = false;
  showModal: boolean = false;
  logInForm!: FormGroup;
  hideShowPass() {
    this.isText = !this.isText;
    this.isText ? (this.eyeIcon = 'fa-eye') : (this.eyeIcon = 'fa-eye-slash');
    this.isText ? (this.type = 'text') : (this.type = 'password');
  }

  ngOnInit(): void {}

    constructor(
      private fb: FormBuilder,
      private authService: AuthService,
      private router: Router,  private toastr: ToastrService,
      private storetoken: StoreTokenService,
    ) {
      this.logInForm = this.fb.group({
        userName: ['', Validators.required],
        password: ['', Validators.required],
      });
  }

  logIn() {
    if (this.logInForm.valid) {
      this.logInModel = this.logInForm.value;
      this.authService.Login(this.logInModel).subscribe({
        next: (res: ResponseDto<LoginResponse>) => {
          if (res.isSuccess == false) {
            this.toastr.error(` ${res.message}`, `Failed`, {
              timeOut: 5000,
            });
          } else {
            this.toastr.success(` ${res.message}`, `Success`, {
              timeOut: 5000,
            });
          };        
          if (res.result.token) {
            this.authService.storeToken(res.result.token);
            this.authService.storeRefreshToken(res.result.refreshToken);

            const tokenPayload = this.authService.decodeToken();
            this.storetoken.setFullNameForStore(tokenPayload.name);
            this.storetoken.setRoleForStore(tokenPayload.role);

            this.toastr.success(
              ` ${res.message}`,
              `Welcome ${tokenPayload.name}!`,
              {
                timeOut: 7000,
              }
            );
            // Navigate to the home page
            this.router.navigate(['/home']);
          } else {

            this.toastr.error(` ${res.message}`, `Error!`, {
              timeOut: 7000,
            });
          }
        }
      });
    } else {

      this.toastr.error(` Invalid Form Data`, `Error!`, {
        timeOut: 7000,
      });
    }
  }


}
