import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
} from '@angular/common/http';

@Injectable()
export class XsrfInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler) {
    // Lấy XSRF Token từ cookie
    const xsrfToken = document.cookie.replace(
      /(?:(?:^|.*;\s*)XSRF-TOKEN\s*=\s*([^;]*).*$)|^.*$/,
      '$1'
    );

    // Thêm X-XSRF-TOKEN vào header
    const cloned = req.clone({
      setHeaders: {
        'X-XSRF-TOKEN': xsrfToken,
      },
    });

    return next.handle(cloned);
  }
}
