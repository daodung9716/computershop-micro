import { Injectable } from '@angular/core';
import {HttpErrorResponse,HttpEvent,HttpHandler,HttpInterceptor,HttpRequest,} from '@angular/common/http';
import { Observable, catchError, finalize, switchMap, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { AuthService } from '../services/auth/auth.service';
import { LoginResponse, TokenApiModel } from '../models/login';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
    private loadingService: LoadingService
  ) {}

  //Interceptor là một tính năng quan trọng trong Angular,nó được dùng
  // để thay đổi hoặc bổ sung các yêu cầu HTTPtrước khi  gửi đi
  //và/hoặc phản hồi trước khi chúng được trả về cho client.

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.loadingService.startLoading();
    const token = localStorage.getItem('token');

    //thêm vào cho request
    if (token) {
      request = request.clone({
        headers: request.headers.set('Authorization', 'Bearer ' + token),
      });
    }
    return next.handle(request).pipe(
      catchError((err: any) => {
        if (err instanceof HttpErrorResponse) {
          //kiểm tra xem biến err có phải là một instance của lớp HttpErrorResponse hay không.
          //Nếu biến err là một instance của lớp HttpErrorResponse, thì điều kiện trả về true
          if (err.status === 401) {
            //chỉ xử lí lỗi 401
            return this.handleUnAuthorizedToken(request, next);
          }
        }
        return throwError(() => err);
      }),
      finalize(() => {
        this.loadingService.stopLoading();
      })
    );
  }

  //xử lí request 401
  handleUnAuthorizedToken(request: HttpRequest<any>, next: HttpHandler) {
    let tokenApiModel = new TokenApiModel();
    tokenApiModel.accessToken = this.authService.getToken()!;
    //thực hiện gán giá trị getToken vào  tokenApiModel.accessToken
    tokenApiModel.refreshToken = this.authService.getRefreshToken()!;
    return this.authService.renewToken(tokenApiModel).pipe(
      switchMap((data: LoginResponse) => {
        this.authService.storeRefreshToken(data.refreshToken);
        this.authService.storeToken(data.token);
        request = request.clone({
          setHeaders: { Authorization: `Bearer ${data.token}` },
        });
        return next.handle(request);
      }),
      catchError((err) => {
        return throwError(() => {
          this.authService.Logout();
          this.toastr.warning('Token is expired', 'Please Login Again');
        });
      })
    );
  }
}
