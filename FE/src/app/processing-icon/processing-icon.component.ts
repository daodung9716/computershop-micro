import { Component, OnInit,  } from '@angular/core';
import { LoadingService } from '../services/loading.service';


@Component({
  selector: 'task-processing-icon',
  templateUrl: './processing-icon.component.html',
  styleUrls: ['./processing-icon.component.css']
})
export class ProcessingIconComponent implements OnInit {
  isLoading:boolean =false;

  constructor(public loadingService: LoadingService) {}

ngOnInit() {
    this.loadingService.isLoading.subscribe((isLoading: boolean) => {
      this.isLoading = isLoading;
    });
  }


}
