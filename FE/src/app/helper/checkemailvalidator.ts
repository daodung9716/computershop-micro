import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable, of, timer } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { AuthService } from '../services/auth/auth.service';
import { ResponseDto } from '../models/responsedto';

export function checkEmailValidatorDebounce(authService: AuthService): AsyncValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    const email = control.value;

    const emailControl = control.root.get('email'); // Lấy ra trường email trong FormGroup
    if ( !emailControl!.dirty) {
      return of(null);
    }
    return timer(800).pipe(
      switchMap(() =>
      authService.checkEmailExist(email).pipe(
          map((response: ResponseDto<string>) => {
            const isEmailExist = response.isSuccess === false;
            return isEmailExist ? { emailExists: true } : null;
          }),
          catchError(() => of(null)) // Handle error, return null to indicate no error
        )
      )
    );
  };
}




