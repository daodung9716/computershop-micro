import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product.component';
import { ShopComponent } from './shop/shop.component';
import { RegisterComponent } from './register/register.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderConfirmComponent } from './order-confirm/order-confirm.component';
import { AdminComponent } from './admin/admin.component';
import { AdminModule } from './admin/admin.module';
import { CategoryComponent } from './admin/category/category.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailComponent } from './order/order-detail/order-detail.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () => AdminModule,
     canActivate: [AuthGuard,],
     canLoad:[AuthGuard,],
  },


  { path: 'login', component: LoginComponent,
  },
  { path: 'register', component: RegisterComponent,
  },

  { path: 'product/:id',
  component: ProductComponent,

  },
  { path: 'shop/category/:id',
  component: ShopComponent,

  },
  { path: 'shop/search/:searchField',
  component: ShopComponent,

  },

  { path: 'home', component: HomeComponent},
  { path: 'product', component: ProductComponent},
  { path: 'shop', component: ShopComponent},
  { path: 'cart', component: CartComponent},
  { path: 'checkout', component: CheckoutComponent},
  { path: 'order', component: OrderComponent, canActivate: [AuthGuard,],},
  { path: 'order/:id', component: OrderDetailComponent, canActivate: [AuthGuard,],},
  { path: 'orderConfirm/:id', component: OrderConfirmComponent, canActivate: [AuthGuard,],},


 { path: '', redirectTo: '/home', pathMatch: 'full' },
 { path: '**', redirectTo: '/home', pathMatch: 'full' },
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
