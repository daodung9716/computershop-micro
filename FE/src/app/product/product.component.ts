import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../services/product/product.service';
import { Product, ProductVM } from '../models/product';
import { CartDto } from '../models/cart';
import { ResponseDto } from '../models/responsedto';
import { CartService } from '../services/cart/cart.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  product!: ProductVM;
  productRelate: ProductVM[] = [];
  quantity: number = 1;

  increaseQuantity() {
    this.quantity++;
  }

  decreaseQuantity() {
    if (this.quantity > 1) {
      this.quantity--;
    }
  }
  constructor(
    private route: ActivatedRoute, private cartService: CartService,
    private productSer: ProductService,
  ) { }
  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');

    this.loadProduct(id$!);

    if (id$ !== null) {

      this.productSer.GetRelate(id$).subscribe((response) => {
        if (response.isSuccess) {
          this.productRelate = response.result;
        } else {
          console.error('Failed to fetch product relate', response.message);
        }
      });
    }
  }

  loadProduct(id: string) {
    if (id !== null) {
      this.productSer.GetByIdProduct(id).subscribe(response => {
        if (response.isSuccess) {
          this.product = response.result;
        } else {
          console.error('Failed to fetch product by id', response.message);
        }
      });
    }
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  CartUpsert(product: ProductVM) {
    const cartDto: CartDto = {
      cartHeader: {},
      cartDetails: [{
        productId: product.id,
        count: 1,
      }]
    }
    this.cartService.CartUpsert(cartDto).subscribe({
      next: (res: ResponseDto<CartDto>) => {

      }, error: (err: any) => {

      }

    })

  }

  CartUpsertDetail(product: ProductVM) {
    const cartDto: CartDto = {
      cartHeader: {},
      cartDetails: [{
        productId: product.id,
        count: this.quantity,
      }]
    }
    this.cartService.CartUpsert(cartDto).subscribe({
      next: (res: ResponseDto<CartDto>) => {

      }, error: (err: any) => {

      }

    })

  }

  tables = [
    { id: 1, name: ' Color', src: 'Blank, Brown, Red' },
    { id: 2, name: ' Material', src: 'Latex' },
    { id: 3, name: '  Weight', src: '55kg' },
  ];
}
