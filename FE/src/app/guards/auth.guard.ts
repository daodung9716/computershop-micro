import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard  {

  constructor(private router: Router,private authService: AuthService, private toastr: ToastrService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree
     {
      if (this.authService.isLoggedIn.value &&this.authService.getToken() ) {
        return true;
      } else {
       // console.log(this.authService.getToken())
        this.toastr.warning('Please Log In First', 'Hello', {
          timeOut: 7000,
        });
        this.router.navigate(['/login']);
        return false;
      }
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.authService.isLoggedIn.value  &&this.authService.getToken()) {
        return true;
      } else {
        this.toastr.warning('Please Log In First', 'Hello', {
          timeOut: 7000,
        });
        this.router.navigate(['/login']);
        return false;
      }
  }
}
