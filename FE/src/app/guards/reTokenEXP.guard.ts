import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot, Route,  Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth/auth.service';
import { SessionService } from '../services/session.service';


@Injectable({
  providedIn: 'root',
})
export class ReFreshTokenGuard {
  constructor(
    private router: Router,
    private authService: AuthService,
    private toastr: ToastrService,
    private sessionService: SessionService,
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.sessionService.checkRefreshTokenExpire()) {
      return true;
    } else {
      this.authService.Logout();
      this.toastr.warning('Please Log In First', 'Hello', {
        timeOut: 7000,
      });
      this.router.navigate(['/login']);
      return false;
    }
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ):
    | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (!this.sessionService.checkRefreshTokenExpire()) {
      return true;
    } else {
      this.authService.Logout();
      this.toastr.warning('Please Log In First', 'Hello', {
        timeOut: 7000,
      });
      this.router.navigate(['/login']);
      return false;
    }
  }
}
