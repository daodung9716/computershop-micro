import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard  {
  constructor(private router: Router,private authService: AuthService, private toastr: ToastrService) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.authService.isLoggedIn.value ) {
        // Nếu isLoggedIn là true, chuyển hướng đến một trang khác
        this.router.navigate(['/home']);
        return false; // Không cho phép điều hướng đến LoginComponent
      }
      return true; // Cho phép điều hướng đến LoginComponent
    }

}
