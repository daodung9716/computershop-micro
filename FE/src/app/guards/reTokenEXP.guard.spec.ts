import { TestBed } from '@angular/core/testing';

import { ReFreshTokenGuard } from './reTokenEXP.guard';

describe('ReFreshTokenGuard', () => {
  let guard: ReFreshTokenGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ReFreshTokenGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
