import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { mergeMap, switchMap, tap, timer } from 'rxjs';
import { Author } from 'src/app/models/author';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  authors: Author[] = [];
  totalPages!: number;
  currentPage: number = 1;
  recordsTotal!: number;
  searchQuery: string = '';
  recordPerPage!: number;
  showModal: boolean = false;
  showModalRemove: boolean = false;
  selectedAuthor!: Author;
  tables = [
    { name: 'Id' },
    { name: 'Name' },

  ]


  constructor(
    private fb: FormBuilder, private productSer: ProductService,
    private router: Router,
    private toastr: ToastrService,
    private loadingService: LoadingService,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.getAuthor();

  }

  getAuthor() {
    this.productSer.GetAuthor(this.currentPage,this.searchQuery).subscribe(response => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.authors = response.result;
        this.recordPerPage = response.recordPerPage;
        this.recordsTotal = response.recordsTotal;
        this.totalPages = response.totalPages;
      } else {
        console.error('Failed to fetch authors:', response.message);
      }
    });

  }

  openModal(taskId: string) {
    this.showModal = true;

  }


  searchProjects() {
    this.currentPage = 1;
    this.getAuthor(); // Reset lại trang về 1
  }

  onSearchInput() {
    timer(300) // Thời gian chờ khi gọi API (milliseconds)
      .pipe(
        switchMap(() => {
          this.currentPage = 1;
          this.getAuthor();
          return timer(0);
        })
      )
      .subscribe();
  }

  getPagesArray() {
    return Array.from({ length: this.totalPages }, (_, index) => index + 1);
  }

  changePage(page: number) {
    if (page < 1 || page > this.totalPages) return;
    this.currentPage = page;
    this.getAuthor();
  }

  deleteCategory(id: string) {
    this.showModalRemove = !this.showModalRemove;
    this.productSer
      .DeleteAuthor(id)
      .pipe(
        tap((res) => {
          if (res.isSuccess == false) {
            this.toastr.error(` ${res.message}`, `Failed`, {
              timeOut: 5000,
            });
          } else {
            this.toastr.success(` ${res.message}`, `Success`, {
              timeOut: 5000,
            });
          }
        }),
        mergeMap(() =>
          this.productSer.GetAuthor(this.currentPage, this.searchQuery)
        )
      )
      .subscribe({
        next: (res) => {
          // Cập nhật lại danh sách user sau khi xóa
          this.authors = res.result;
          this.recordPerPage=res.recordPerPage;
          this.recordsTotal=res.recordsTotal;
          this.totalPages=res.totalPages;
        },
        error: (error) => {
        //   console.error(error);
        //   this.toastr.error('Error  receive task', 'ERROR', {
        //     timeOut: 5000,
        //   });
         },
      });
  }
}
