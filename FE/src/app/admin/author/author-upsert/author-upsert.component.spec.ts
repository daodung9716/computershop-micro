import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorUpsertComponent } from './author-upsert.component';

describe('AuthorUpsertComponent', () => {
  let component: AuthorUpsertComponent;
  let fixture: ComponentFixture<AuthorUpsertComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthorUpsertComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AuthorUpsertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
