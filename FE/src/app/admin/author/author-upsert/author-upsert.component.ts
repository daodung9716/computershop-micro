import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { finalize, tap } from 'rxjs';
import { Author } from 'src/app/models/author';
import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';


@Component({
  selector: 'app-author-upsert',
  templateUrl: './author-upsert.component.html',
  styleUrls: ['./author-upsert.component.css']
})
export class AuthorUpsertComponent implements OnInit {
  isDisabled = true;
  selectedImage: File | null = null;
  authorForm!: FormGroup;
  authorData!: Author;
  showModal: boolean = false;
  idUpdate!: string | null;
  detailCategories = [
    {
      controlName: 'name',
      name: 'Name',
      type: 'text',
      error: [{ key: 'required', message: 'Name is required' },
      { key: 'minlength', message: 'Name is min length 3 characters' },
      ]
    }
  ];

  constructor(
    private fb: FormBuilder, private productSer: ProductService,
    private route: ActivatedRoute, private router: Router,
    private loadingService: LoadingService, private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');

    this.idUpdate = id$ === '0' ? null : id$;

    this.authorForm = this.fb.group({
      id: [''],
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
    });

    if (this.idUpdate !== null) {
      this.productSer.DetailAuthor(this.idUpdate).subscribe({
        next: (res: ResponseDto<Author>) => {

          if (res) {
            this.authorForm.patchValue({
              id: res.result.id,
              name: res.result.name,
            });
          }
        },
        error: (error) => {
          console.error('Error getting detail author', error);
        },
      });
    } else {

    }
  }



  CreateAuthor() {
    if (this.authorForm.valid) {
      this.authorData = this.authorForm.value;
      delete this.authorData.id;


      this.productSer
        .UpsertAuthor(this.authorData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['author'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }
  }

  UpdateAuthor() {
    if (this.authorForm.valid) {
      this.authorData = this.authorForm.value;

      this.productSer
        .UpsertAuthor(this.authorData)
        .pipe(
          tap((res) => {

            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['author'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }

  }
}
