import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { finalize, tap } from 'rxjs';
import { Category } from 'src/app/models/category';
import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-category-upsert',
  templateUrl: './category-upsert.component.html',
  styleUrls: ['./category-upsert.component.css']
})
export class CategoryUpsertComponent implements OnInit {
  isDisabled = true;
  selectedImage: File | null = null;
  categoryForm!: FormGroup;
  categoryData!: Category;
  showModal: boolean = false;
  srcImage!:string;
  idUpdate!: string | null;
  detailCategories = [
    {
      controlName: 'name',
      name: 'Name',
      type: 'text',
      error: [{ key: 'required', message: 'Name is required' },
      { key: 'minlength', message: 'Name is min length 3 characters' },
      ]
    },
    {
      controlName: 'displayOrder',
      name: 'Display Order',
      type: 'number',
      error: [{ key: 'required', message: 'Display Order is required' },
      ]
    }
  ];

  constructor(
    private fb: FormBuilder, private productSer: ProductService,
    private route: ActivatedRoute, private router: Router,
    private loadingService: LoadingService, private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');

    this.idUpdate = id$ === '0' ? null : id$;

    this.categoryForm = this.fb.group({
      id: [''],
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      displayOrder: [0, Validators.compose([Validators.required])],
      image: ['']
    });

    if (this.idUpdate !== null) {
      this.productSer.DetailCategory(this.idUpdate).subscribe({
        next: (res: ResponseDto<Category>) => {

          if (res) {
            this.categoryForm.patchValue({
              id: res.result.id,
              name: res.result.name,
              displayOrder: res.result.displayOrder,
            });
          }
         this.srcImage=res.result.image!
        },
        error: (error) => {
          console.error('Error getting detail category', error);
        },
      });
    } else {
      console.log('Loading Create User');
    }
  }

  onFileSelected(event: any) {
    this.selectedImage = event.target.files[0] as File;
  }

  CreateCategory() {
    if (this.categoryForm.valid) {
      this.categoryData = this.categoryForm.value;
      const formData = new FormData();
      delete this.categoryData.id;

      const imagePath = 'assets/images/category/';
      this.categoryData.image = imagePath;

      if (this.selectedImage) {
        formData.append('imageFile', this.selectedImage);
      }

      formData.append('categoryJson', JSON.stringify(this.categoryData));

      this.productSer
        .UpsertCategory(formData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['category'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }
  }

  UpdateCategory() {
    if (this.categoryForm.valid) {
      this.categoryData = this.categoryForm.value;
      const formData = new FormData();

      const imagePath = 'assets/images/category/';
      this.categoryData.image = imagePath;

      if (this.selectedImage) {
        formData.append('imageFile', this.selectedImage);
      }

      formData.append('categoryJson', JSON.stringify(this.categoryData));

      this.productSer
        .UpsertCategory(formData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['category'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }

  }
}
