import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/models/category';
import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent implements OnInit {
  isDisabled = true;
  categoryForm!: FormGroup;
  showModal: boolean = false;
  detailCategories = [
    {
      controlName: 'name',
      name: 'Name',
    },
    {
      controlName: 'displayOrder',
      name: 'Display Order',
    }
  ];

  constructor(
    private fb: FormBuilder,private productSer: ProductService,
    private route: ActivatedRoute,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');
    this.categoryForm = this.fb.group({
      id: [{ value: 0, disabled: true }],
      name: [{ value: '', disabled: true }],
      displayOrder: [{ value: 0, disabled: true }],
    });

    if (id$ !== null) {
      this.productSer.DetailCategory(id$).subscribe({
        next: (res :ResponseDto<Category>) => {
          this.categoryForm.patchValue({
            name: res.result.name,
            displayOrder: res.result.displayOrder,
          });
        },
        error: (error) => {
          console.error('Error getting detail category', error);
        },
      });
    } else {
      console.error('Error getting detail category');
    }
  }
}
