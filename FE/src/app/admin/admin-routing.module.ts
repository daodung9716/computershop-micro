import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';
import { CategoryComponent } from './category/category.component';
import { CompanyComponent } from './company/company.component';
import { ProductAdminComponent } from './productadmin/productadmin.component';
import { AdminComponent } from './admin.component';
import { AuthorComponent } from './author/author.component';
import { CategoryDetailComponent } from './category/category-detail/category-detail.component';
import { CategoryUpsertComponent } from './category/category-upsert/category-upsert.component';
import { AuthorUpsertComponent } from './author/author-upsert/author-upsert.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserUpsertComponent } from './user/user-upsert/user-upsert.component';
import { ProductUpsertComponent } from './productadmin/product-upsert/product-upsert.component';
import { ProductDetailComponent } from './productadmin/product-detail/product-detail.component';

const routes: Routes = [

  { path: 'detailCategory/:id',component: CategoryDetailComponent,outlet:"detail"},

  { path: 'upsertCategory/:id',component: CategoryUpsertComponent,outlet:"detail"},

  { path: 'detailUser/:id',component: UserDetailComponent,outlet:"detail"},

  { path: 'detailProduct/:id',component: ProductDetailComponent,outlet:"detail"},

  { path: 'upsertUser/:id',component: UserUpsertComponent,outlet:"detail"},

  { path: 'upsertProduct/:id',component: ProductUpsertComponent,outlet:"detail"},

  { path: 'upsertAuthor/:id',component: AuthorUpsertComponent,outlet:"detail"},


  { path: '', component: AdminComponent,},

{ path: 'category', component: CategoryComponent,outlet:"detail"},
  { path: 'company', component: CompanyComponent,outlet:'detail'},
  { path: 'user', component: UserComponent,outlet:'detail'},
  { path: 'productAdmin', component: ProductAdminComponent,outlet:'detail'},
  { path: 'author', component: AuthorComponent,outlet:'detail'},


]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
