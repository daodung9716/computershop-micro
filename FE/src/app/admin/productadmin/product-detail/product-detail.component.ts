import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductVM } from 'src/app/models/product';

import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  isDisabled = true;
  productForm!: FormGroup;
  showModal: boolean = false;
  detailCategories = [
    {
      controlName: 'name',
      name: 'Name',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'price',
      name: 'Price',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'listPrice',
      name: 'List Price',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'buyCount',
      name: 'Buy Count',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'description',
      name: 'Description',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'authorName',
      name: 'Buy Count',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'categoryName',
      name: 'Description',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    }
  ];

  constructor(
    private fb: FormBuilder,private productSer: ProductService,
    private route: ActivatedRoute,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');
    this.productForm = this.fb.group({
      id: [{ value: 0, disabled: true }],
      name: [{ value: '', disabled: true }],
      price: [{ value: 0, disabled: true }],
      listPrice: [{ value: 0, disabled: true }],
      buyCount: [{ value: '', disabled: true }],
      description: [{ value: 0, disabled: true }],
      authorName: [{ value: 0, disabled: true }],
      categoryName: [{ value: '', disabled: true }],
    });

    if (id$ !== null) {
      this.productSer.GetByIdProduct(id$).subscribe({
        next: (res :ResponseDto<ProductVM>) => {
          this.productForm.patchValue({
            id: res.result.id,
            name: res.result.name,
            price: res.result.price,
            buyCount: res.result.buyCount,
            listPrice: res.result.listPrice,
            description: res.result.description,
            authorName: res.result.authorName,
            categoryName: res.result.categoryName,

          });
        },
        error: (error) => {
          console.error('Error getting detail product', error);
        },
      });
    } else {
      console.error('Error getting detail product');
    }
  }
}
