import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { finalize, mergeMap, tap } from 'rxjs';
import { ProductDto, ProductImage, ProductVM } from 'src/app/models/product';
import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-product-upsert',
  templateUrl: './product-upsert.component.html',
  styleUrls: ['./product-upsert.component.css']
})
export class ProductUpsertComponent implements OnInit {
  isDisabled = true;
  selectedImages: File[] = [];
  selectedImageNames: string[] = [];
  productForm!: FormGroup;
  productData!: ProductDto;
  showModal: boolean = false;
  srcImage!: string;
  idUpdate!: string | null;
  productImages: ProductImage[] = [];
  detailProductss = [
    {
      controlName: 'name',
      name: 'Name',
      type: 'text',
      class: ' p-2 text-center w-1/4 hidden xs:block',
      error: [{ key: 'required', message: 'Name is required' },
      { key: 'minlength', message: 'Name is min length 3 characters' },
      ]
    },
    {
      controlName: 'price',
      name: 'Price',
      type: 'number',
      class: ' p-2 text-center w-1/4 hidden xs:block',
      error: [{ key: 'required', message: 'Price is required' },
      ]
    },
    {
      controlName: 'listPrice',
      name: 'List Price',
      type: 'number',
      class: ' p-2 text-center w-1/4 hidden xs:block',
      error: [{ key: 'required', message: 'List Price is required' },
      ]
    },
    {
      controlName: 'buyCount',
      name: 'Buy Count',
      type: 'number',
      class: ' p-2 text-center w-1/4 hidden xs:block',
      error: [{ key: 'required', message: 'Buy Count is required' },
      ]
    },
    {
      controlName: 'description',
      name: 'Description',
      type: 'text',
      class: ' p-2 text-center w-1/4 hidden xs:block',
      error: [{ key: 'required', message: 'Description is required' },
      ]
    }
  ];

  authors = [
    { id: 1, name: 'AMD' },
    { id: 2, name: 'INTEL' },
    { id: 3, name: 'MSI' },
    { id: 4, name: 'ASROCK' },
    { id: 5, name: 'ASUS' },
    { id: 6, name: 'GIGABYTE' },
    { id: 7, name: 'HKC' },
    { id: 8, name: 'AOC' },
    { id: 9, name: 'E-DRA' },
    { id: 10, name: 'DAREU' },
    { id: 11, name: 'CORSAIR' },
  ];

  categories = [
    { id: 1, name: 'Mainboard' },
    { id: 2, name: 'CPU' },
    { id: 3, name: 'VGA' },
    { id: 4, name: 'Monitor' },
    { id: 5, name: 'Keyboard' },
    { id: 6, name: 'Mouse' },
  ];

  constructor(
    private fb: FormBuilder, private productSer: ProductService,
    private route: ActivatedRoute, private router: Router,
    private loadingService: LoadingService, private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');

    this.idUpdate = id$ === '0' ? null : id$;

    this.productForm = this.fb.group({
      id: [''],
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      price: [0, Validators.compose([Validators.required])],
      buyCount: [0, Validators.compose([Validators.required])],
      listPrice: [0, Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],
      authorId: ['', Validators.compose([Validators.required])],
      categoryId: ['', Validators.compose([Validators.required])],
    });

    if (this.idUpdate !== null) {
      this.productSer.GetByIdProduct(this.idUpdate).subscribe({
        next: (res: ResponseDto<ProductVM>) => {
          if (res) {
            this.productForm.patchValue({
              id: res.result.id,
              name: res.result.name,
              price: res.result.price,
              buyCount: res.result.buyCount,
              listPrice: res.result.listPrice,
              description: res.result.description,
              authorId: res.result.authorId,
              categoryId: res.result.categoryId,

            });

            this.productImages = res.result.productImages
          }
        },
        error: (error) => {
          console.error('Error getting detail product', error);
        },
      });
    } else {
      console.log('Loading Create User');
    }
  }



  Print() {
    console.log(this.productForm.value)
  }

  onFileSelected(event: any) {

    // Lấy danh sách các tệp ảnh đã chọn và thêm vào mảng selectedImages
    for (let i = 0; i < event.target.files.length; i++) {
      this.selectedImages.push(event.target.files[i]);
      this.selectedImageNames.push(event.target.files[i].name);
    }


  }

  CreateProduct() {
    if (this.productForm.valid) {
      this.productData = this.productForm.value;
      const formData = new FormData();
      delete this.productData.id;

      if (this.selectedImages && this.selectedImages.length > 0) {
        for (let i = 0; i < this.selectedImages.length; i++) {
          formData.append('imageFiles', this.selectedImages[i]);
        }
      }

      formData.append('productDtoJson', JSON.stringify(this.productData));

      this.productSer
        .UpsertProduct(formData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['productAdmin'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }
  }

  UpdateProduct() {
    if (this.productForm.valid) {
      this.productData = this.productForm.value;
      const formData = new FormData();


      if (this.selectedImages && this.selectedImages.length > 0) {
        for (let i = 0; i < this.selectedImages.length; i++) {
          formData.append('imageFiles', this.selectedImages[i]);
        }
      }
      formData.append('productDtoJson', JSON.stringify(this.productData));
      this.productSer
        .UpsertProduct(formData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['productAdmin'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }

  }

  RemoveImage(index: number) {
    // Xóa tệp ảnh khỏi danh sách selectedImages và selectedImageNames
    this.selectedImages.splice(index, 1);
    this.selectedImageNames.splice(index, 1);
  }

  DeleteProductImage(id: string) {
    this.productSer
      .DeleteProductImage(id)
      .pipe(
        tap((res) => {
          if (res.isSuccess == false) {
            this.toastr.error(` ${res.message}`, `Failed`, {
              timeOut: 5000,
            });
          } else {
            this.toastr.success(` ${res.message}`, `Success`, {
              timeOut: 5000,
            });
          }
        }),
        mergeMap(() =>
          this.productSer.GetByIdProduct(this.idUpdate!)
        )
      )
      .subscribe({
        next: (res) => {
          this.productImages = res.result.productImages
        },
        error: (error) => {

        },
      });
  }
}
