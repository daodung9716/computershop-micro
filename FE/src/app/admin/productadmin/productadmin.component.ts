import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { mergeMap, switchMap, tap, timer } from 'rxjs';
import { Category } from 'src/app/models/category';
import { ProductVM } from 'src/app/models/product';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-productadmin',
  templateUrl: './productadmin.component.html',
  styleUrls: ['./productadmin.component.css']
})
export class ProductAdminComponent {
  products: ProductVM[] = [];
  totalPages!: number;
  currentPage: number = 1;
  recordsTotal!: number;
  searchQuery: string = '';
  recordPerPage!: number;
  showModal: boolean = false;
  showModalRemove: boolean = false;
  selectedProduct!: ProductVM;
  tables = [
    {  name: 'Name'},
    {  name: 'Price'},
    {  name: 'List Price'},
    {  name: 'Author Name'},
    {  name: 'Category Name'},

  ]


  constructor(
    private fb: FormBuilder, private productSer: ProductService,
    private router: Router,
    private toastr: ToastrService,
    private loadingService: LoadingService,
    private authService:AuthService,
  ) {}

  ngOnInit(): void {

   this.GetProduct();

  }

  GetProduct(){
    this.productSer.GetProduct(this.currentPage,this.searchQuery).subscribe(response => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.products = response.result;
        this.recordPerPage=response.recordPerPage;
        this.recordsTotal=response.recordsTotal;
        this.totalPages=response.totalPages;

      } else {
        console.error('Failed to fetch products:', response.message);
      }
    });
  }

  OpenModal(taskId: string) {
    this.showModal=true;

  }


  SearchProjects() {
    this.currentPage = 1;
    this.GetProduct(); // Reset lại trang về 1
  }

  OnSearchInput() {
    timer(300) // Thời gian chờ khi gọi API (milliseconds)
      .pipe(
        switchMap(() => {
          this.currentPage = 1;
          this.GetProduct();
          return timer(0); // Optional: Delay for API response (if needed)
        })
      )
      .subscribe();
  }

  GetPagesArray() {
    return Array.from({ length: this.totalPages }, (_, index) => index + 1);
  }

  ChangePage(page: number) {
    if (page < 1 || page > this.totalPages) return;
    this.currentPage = page;
    this.GetProduct();
  }

  DeleteProduct(id: string) {
    this.showModalRemove = !this.showModalRemove;
    this.productSer
      .DeleteProduct(id)
      .pipe(
        tap((res) => {
          if (res.isSuccess == false) {
            this.toastr.error(` ${res.message}`, `Failed`, {
              timeOut: 5000,
            });
          } else {
            this.toastr.success(` ${res.message}`, `Success`, {
              timeOut: 5000,
            });
          }
        }),
        mergeMap(() =>
          this.productSer.GetProduct(this.currentPage, this.searchQuery)
        )
      )
      .subscribe({
        next: (res) => {
          // Cập nhật lại danh sách user sau khi xóa
          this.products = res.result;
          this.recordPerPage=res.recordPerPage;
          this.recordsTotal=res.recordsTotal;
          this.totalPages=res.totalPages;
        },
        error: (error) => {

         },
      });
  }
}
