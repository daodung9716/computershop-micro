import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserDto } from 'src/app/models/login';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  isDisabled = true;
  userForm!: FormGroup;
  showModal: boolean = false;
  detailUsers = [
    {
      controlName: 'name',
      name: 'Name',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'email',
      name: 'Email',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'phoneNumber',
      name: 'Phone Number',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'address',
      name: 'Address',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    },
    {
      controlName: 'role',
      name: 'Role',
      class: ' p-2 text-center w-1/4 hidden xs:block',
    }
  ];

  constructor(
    private fb: FormBuilder,private authSer: AuthService,
    private route: ActivatedRoute,
    private loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');
    this.userForm = this.fb.group({
      id: [{ value: 0, disabled: true }],
      name: [{ value: '', disabled: true }],
      email: [{ value: 0, disabled: true }],
      phoneNumber: [{ value: '', disabled: true }],
      address: [{ value: 0, disabled: true }],
      role: [{ value: 0, disabled: true }],
    });

    if (id$ !== null) {
      this.authSer.GetByIdUser(id$).subscribe({
        next: (res :ResponseDto<UserDto>) => {
          this.userForm.patchValue({
            name: res.result.name,
            email: res.result.email,
            phoneNumber: res.result.phoneNumber,
            address: res.result.address,
            role: res.result.role,
          });
          console.log(this.userForm.value)
          console.log(res.result)
        },
        error: (error) => {
          console.error('Error getting detail user', error);
        },
      });
    } else {
      console.error('Error getting detail user');
    }
  }
}
