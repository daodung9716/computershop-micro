import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { finalize, tap } from 'rxjs';
import { checkEmailValidatorDebounce } from 'src/app/helper/checkemailvalidator';
import { UserDto } from 'src/app/models/login';
import { RegisterDTO } from 'src/app/models/register';
import { ResponseDto } from 'src/app/models/responsedto';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingService } from 'src/app/services/loading.service';


@Component({
  selector: 'app-user-upsert',
  templateUrl: './user-upsert.component.html',
  styleUrls: ['./user-upsert.component.css']
})
export class UserUpsertComponent implements OnInit {
  isDisabled = true;
  selectedImage: File | null = null;
  userForm!: FormGroup;
  userData!: UserDto;
  registerData!: RegisterDTO;
  showModal: boolean = false;
  eyeIcon: string = 'fa-eye-slash';
  isText: boolean = false;
  type: string = 'password';
  emailMessageError!: string;
  idUpdate!: string | null;

  inputOther = [
    {
      name: 'name',
      placeholder: 'Name',
      type: 'text',
      errors: [
        { key: 'required', message: 'Name is required' },
        { key: 'minlength', message: 'Name must have at least 3 characters' },
      ],
    },
    {
      name: 'address',
      placeholder: 'Address',
      type: 'text',
      errors: [
        { key: 'required', message: 'Address is required' },
        {
          key: 'minlength',
          message: 'Address must have at least 6 characters',
        }
      ],
    },
    {
      name: 'phoneNumber',
      placeholder: 'Phone Number',
      type: 'number',
      errors: [
        { key: 'required', message: 'PhoneNumber is required' },
        {
          key: 'minlength',
          message: 'PhoneNumber must have  10 numbers',
        },
        {
          key: 'maxlength',
          message: 'PhoneNumber must have  10 numbers',
        },
      ],
    },
  ];


  passwordErrors = [
    { name: 'password', key: 'required', message: 'Password is required' },
    {
      name: 'password',
      key: 'minlength',
      message: 'Password must have at least 8 characters',
    },
    {
      name: 'password',
      key: 'maxlength',
      message: 'Password must have at most 20 characters',
    },
    {
      name: 'password',
      key: 'pattern',
      message: 'Password must have at least 1 uppercase, lowercase, and digit',
    },
  ];

  constructor(
    private fb: FormBuilder, private authSer: AuthService,
    private route: ActivatedRoute, private router: Router,
    private loadingService: LoadingService, private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');

    this.idUpdate = id$ === '0' ? null : id$;

    this.userForm = this.fb.group({
      id:[''],
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ['',
        [Validators.required], checkEmailValidatorDebounce(this.authSer)],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8),
      Validators.maxLength(20),
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$')])],
      phoneNumber: [0, Validators.compose([Validators.required, Validators.minLength(10),
      Validators.maxLength(10),])],
      address: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      role: ['', Validators.required]
    });

    if (this.idUpdate !== null) {
      const emailControl = this.userForm.get('email');
      const password = this.userForm.get('password');
      if (emailControl && password) {
        this.userForm.removeControl('email');
        this.userForm.removeControl('password');
      }
      this.authSer.GetByIdUser(this.idUpdate).subscribe({
        next: (res: ResponseDto<UserDto>) => {
          if (res) {
            this.userForm.patchValue({
              id: res.result.id,
              name: res.result.name,
              phoneNumber: res.result.phoneNumber,
              address: res.result.address,
              role: res.result.role
            });

          }

        },
        error: (error) => {
          console.error('Error getting detail user', error);
        },
      });
    } else {
      console.log('Loading Create User');
    }


  }

  onCheckEmail() {
    const email = this.userForm.get('email')!.value;

    if (email) {
      this.authSer.checkEmailExist(email).subscribe({
        next: (res: ResponseDto<string>) => {
          this.emailMessageError = res.message;
          console.log(this.emailMessageError)
        },
        error: (error) => {
          console.error('Error check email existed', error);
        },
      });
    } else { }
  }

  getInvalidFields(): string[] {
    const invalidFields: string[] = [];

    // Lặp qua các FormControl trong FormGroup
    for (const field in this.userForm.controls) {
      if (this.userForm.controls[field].invalid) {
        invalidFields.push(field);
      }
    }
    console.log(this.userForm.value)
    return invalidFields;
  }

  CreateUser() {
    if (this.userForm.valid) {
      this.userData = this.userForm.value;

      delete this.userData.id;

      this.authSer
        .register(this.userData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['user'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }
  }

  UpdateUser() {
    if (this.userForm.valid) {
      this.userData = this.userForm.value;

      this.authSer
        .UpdateUser(this.userData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/admin', { outlets: { detail: ['user'] } }]);
          }),
          finalize(() => { })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }

  }

  hideShowPass() {
    this.isText = !this.isText;
    this.isText ? (this.eyeIcon = 'fa-eye') : (this.eyeIcon = 'fa-eye-slash');
    this.isText ? (this.type = 'text') : (this.type = 'password');
  }
}
