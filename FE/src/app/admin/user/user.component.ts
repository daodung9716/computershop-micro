import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, mergeMap, switchMap, tap, timer } from 'rxjs';
import { UserDto } from 'src/app/models/login';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: UserDto[]=[];
  totalPages!: number;
  currentPage: number = 1;
  recordsTotal!: number;
  searchQuery: string = '';
  recordPerPage!: number;
  showModal: boolean = false;
  showModalRemove: boolean = false;
  selectedUsers!: UserDto;
  tables = [
    {  name: 'Name'},
    {  name: 'Email'},
    {  name: 'Phone Number'},
    {  name: 'Address'},

  ]


  constructor(
    private fb: FormBuilder, private productSer: ProductService,
    private router: Router,
    private toastr: ToastrService,
    private loadingService: LoadingService,
    private authService:AuthService,
  ) {}

  ngOnInit(): void {

   this.getUser();


  }

  getUser(){
    this.authService.GetUsers(this.currentPage,this.searchQuery).subscribe(response => {
      if (response.isSuccess) {
        this.users = response.result;
        this.recordPerPage=response.recordPerPage;
        this.recordsTotal=response.recordsTotal;
        this.totalPages=response.totalPages;
      } else {
        console.error('Failed to fetch list users:', response.message);
      }
    });
  }

  openModal(taskId: string) {
    this.showModal=true;

  }


  searchProjects() {
    this.currentPage = 1;
    this.getUser(); // Reset lại trang về 1
  }

  onSearchInput() {
    timer(300) // Thời gian chờ khi gọi API (milliseconds)
      .pipe(
        switchMap(() => {
          this.currentPage = 1;
          this.getUser();
          return timer(0); // Optional: Delay for API response (if needed)
        })
      )
      .subscribe();
  }

  getPagesArray() {
    return Array.from({ length: this.totalPages }, (_, index) => index + 1);
  }

  changePage(page: number) {
    if (page < 1 || page > this.totalPages) return;
    this.currentPage = page;
    this.getUser();
  }

  deleteUser(id: string) {
    this.showModalRemove = !this.showModalRemove;
    this.authService
      .DeleteUser(id)
      .pipe(
        tap((res) => {
          if (res.isSuccess == false) {
            this.toastr.error(` ${res.message}`, `Failed`, {
              timeOut: 5000,
            });
          } else {
            this.toastr.success(` ${res.message}`, `Success`, {
              timeOut: 5000,
            });
          }
        }),
        mergeMap(() =>
          this.authService.GetUsers(this.currentPage, this.searchQuery)
        )
      )
      .subscribe({
        next: (res) => {
          // Cập nhật lại danh sách user sau khi xóa
          this.users = res.result;
          this.recordPerPage=res.recordPerPage;
          this.recordsTotal=res.recordsTotal;
          this.totalPages=res.totalPages;
        },
        error: (error) => {

         },
      });
  }


}
