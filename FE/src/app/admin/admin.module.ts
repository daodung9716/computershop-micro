import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductAdminComponent } from './productadmin/productadmin.component';
import { CompanyComponent } from './company/company.component';
import { CategoryComponent } from './category/category.component';
import { UserComponent } from './user/user.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { RouterModule } from '@angular/router';
import { AuthorComponent } from './author/author.component';
import { CategoryDetailComponent } from './category/category-detail/category-detail.component';
import { CategoryUpsertComponent } from './category/category-upsert/category-upsert.component';

import { AuthorUpsertComponent } from './author/author-upsert/author-upsert.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { UserUpsertComponent } from './user/user-upsert/user-upsert.component';
import { BrowserModule } from '@angular/platform-browser';
import { ProductUpsertComponent } from './productadmin/product-upsert/product-upsert.component';
import { ProductDetailComponent } from './productadmin/product-detail/product-detail.component';



@NgModule({
  declarations: [
    ProductAdminComponent,
    CompanyComponent,
    UserComponent,
    AdminComponent,
    CategoryComponent,
    AuthorComponent,
    CategoryDetailComponent,
    CategoryUpsertComponent,
    AuthorUpsertComponent,
    UserDetailComponent,
    UserUpsertComponent,
    ProductUpsertComponent,
    ProductDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
