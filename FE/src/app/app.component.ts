import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from './services/product/product.service';
import { Category } from './models/category';
import { AuthService } from './services/auth/auth.service';
import { StoreTokenService } from './services/auth/storetoken.service';
import { LoadingService } from './services/loading.service';
import { CartService } from './services/cart/cart.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'ecommerce';
  categories: Category[] = [];
  public fullName: string = "";
  public role: string = "";
  searchTerm: string = '';
  isLoggedInS: boolean = false;
  socials = [
    { id: 1, class: 'fa-brands fa-facebook-square fa-2xl' },
    { id: 2, class: 'fa-brands fa-instagram-square fa-2xl' },
    { id: 3, class: 'fa-brands fa-twitter-square fa-2xl' },
    { id: 4, class: 'fa-brands fa-github-square fa-2xl' },

  ];

  footer1 = [
    { id: 1, name: 'Marketing' },
    { id: 2, name: 'Analitics' },
    { id: 3, name: 'Commerce' },
    { id: 4, name: 'Insights' },
  ];

  footer2 = [
    { id: 1, name: 'Pricing' },
    { id: 2, name: 'Documentation' },
    { id: 3, name: 'Guides' },
    { id: 4, name: 'API Status' },
  ];

  constructor(private router: Router, private productSer: ProductService, private authService: AuthService,
    private storeToken: StoreTokenService, private loadingService: LoadingService) { }

  ngOnInit() {
    this.storeToken.getFullNameFromStore().subscribe(value => {
      //lấy fullName từ TOKEN
      let fullNameFromToken = this.authService.getFullNameFromToken();
      this.fullName = value || fullNameFromToken
    })

    this.storeToken.getRoleFromStore().subscribe(value => {
      let roleFromToken = this.authService.getRoleFromToken();
      this.role = value || roleFromToken
    })

    this.authService.isLoggedIn.subscribe((isLoggedIn: boolean) => {
      this.isLoggedInS = isLoggedIn;
    });

    this.productSer.GetCategory(1, '').subscribe(response => {
      if (response.isSuccess) {
        // Gán giá trị từ response.result vào categoryList
        this.categories = response.result;
      } else {
        console.error('Failed to fetch categories:', response.message);
      }
    });

  }

  onSearchButtonClick() {
    // Sử dụng Router để chuyển hướng đến URL mới với giá trị searchTerm
    this.router.navigate(['/shop/search', this.searchTerm]);

  }

  Logout() {
    this.authService.Logout();
    this.isLoggedInS = false;
  }




}
