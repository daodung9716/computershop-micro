import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../services/order/order.service';
import { ResponseDto } from '../models/responsedto';
import { OrderHeaderDto } from '../models/order';
import { CartService } from '../services/cart/cart.service';

@Component({
  selector: 'app-order-confirm',
  templateUrl: './order-confirm.component.html',
  styleUrls: ['./order-confirm.component.css']
})
export class OrderConfirmComponent implements OnInit {

  constructor(
    private route: ActivatedRoute, private orderService: OrderService,
    private router: Router,private cartService: CartService
  ) { }


  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');
    const idNumber = parseInt(id$!, 10);
    this.orderService.ValidateStripeSession(idNumber).subscribe({
      next: (res: ResponseDto<OrderHeaderDto>) => {
      }
    })

    this.cartService.RemoveCart().subscribe({
      next: (res: ResponseDto<string>) => {
      }
    })


  }

}
