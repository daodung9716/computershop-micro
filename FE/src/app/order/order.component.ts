import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoadingService } from 'src/app/services/loading.service';
import { ProductService } from 'src/app/services/product/product.service';
import { OrderHeaderDto } from '../models/order';
import { OrderService } from '../services/order/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orders: OrderHeaderDto[]=[];
  totalPages!: number;
  currentPage: number = 1;
  recordsTotal!: number;
  searchQuery: string = '';
  recordPerPage!: number;
  showModal: boolean = false;
  showModalRemove: boolean = false;
  selectedOrders!: OrderHeaderDto;
  tables = [
    {  name: 'Order Id'},
    {  name: 'Name'},
    {  name: 'Email'},
    {  name: 'Phone Number'},
    {  name: 'Address'},
    {  name: 'Status'},
    {  name: 'Total'},

  ]

  constructor(
    private fb: FormBuilder, private orderSer: OrderService,
    private router: Router,
    private toastr: ToastrService,
    private loadingService: LoadingService,
    private authService:AuthService,
  ) {}

  ngOnInit(): void {
   this.GetOrder();
  }

  GetOrder(){
    this.orderSer.GetAllOrders().subscribe(response => {
      if (response.isSuccess) {
        this.orders = response.result;
        this.recordPerPage=response.recordPerPage;
        this.recordsTotal=response.recordsTotal;
        this.totalPages=response.totalPages;
      } else {
        console.error('Failed to fetch list orders:', response.message);
      }
    });
  }

  openModal(taskId: string) {
    this.showModal=true;

  }

  getPagesArray() {
    return Array.from({ length: this.totalPages }, (_, index) => index + 1);
  }

  changePage(page: number) {
    if (page < 1 || page > this.totalPages) return;
    this.currentPage = page;
    this.GetOrder();
  }


}
