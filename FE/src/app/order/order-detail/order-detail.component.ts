import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ResponseDto } from 'src/app/models/responsedto';
import { LoadingService } from 'src/app/services/loading.service';
import { OrderService } from 'src/app/services/order/order.service';
import { OrderDetailsDto, OrderHeaderDto, OrderHeaderUpdate, StripeRequestDto } from 'src/app/models/order';
import { DatePipe } from '@angular/common';
import { mergeMap, tap } from 'rxjs';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  sDisabled = true;
  id: string = '';
  orderForm!: FormGroup;
  showModal: boolean = false;
  orderHeaderData!: OrderHeaderUpdate;
  orderHeaderResponse!:OrderHeaderDto;
  detailOrderDto: OrderDetailsDto[] = [];
  status: string = '';
  detailOrders = [
    {
      controlName: 'name',
      name: 'Name',
      errors: [
        { key: 'required', message: 'Name is required' },
        { key: 'minlength', message: 'Name must have at least 3 characters' },
      ],
    },
    {
      controlName: 'email',
      name: 'Email',
      errors: [
        { key: 'required', message: 'Email is required' },
      ],
    },
    {
      controlName: 'phone',
      name: 'Phone Number',
      errors: [
        { key: 'required', message: 'Phone Number is required' },
        { key: 'minlength', message: 'Phone Number must have at least 10 characters' },
        { key: 'maxlength', message: 'Phone Number must have at least 10 characters' },
      ],
    },
    {
      controlName: 'address',
      name: 'Address',
      errors: [
        { key: 'required', message: 'Address is required' },
        { key: 'minlength', message: 'Address must have at least 6 characters' },
      ],
    },
    {
      controlName: 'orderTime',
      name: 'Order Time'
    },
    {
      controlName: 'status',
      name: 'Status'
    }
  ];

  constructor(
    private fb: FormBuilder, private orderSer: OrderService, private datePipe: DatePipe,  private toastr: ToastrService,
    private route: ActivatedRoute, private loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    const id$ = this.route.snapshot.paramMap.get('id');

    this.orderForm = this.fb.group({
      orderHeaderId: [''],
      name: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      email: ['', [Validators.required]],
      phone: [0, Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10),])],
      address: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      orderTime: [{ value: '', disabled: true }],
      status: [{ value: '', disabled: true }],
    });

    if (id$ !== null) {
      this.id = id$;
      this.orderSer.GetOrder(id$).subscribe({
        next: (res: ResponseDto<OrderHeaderDto>) => {
          const formattedOrderTime = this.datePipe.transform(res.result.orderTime, 'dd/MM/yyyy');
          this.orderForm.patchValue({
            orderHeaderId: res.result.orderHeaderId,
            name: res.result.name,
            email: res.result.email,
            phone: res.result.phone,
            address: res.result.address,
            orderTime: formattedOrderTime,
            status: res.result.status,
          });
          this.detailOrderDto = res.result.orderDetails;
          this.status = res.result.status!;
          this.orderHeaderResponse=res.result;
console.log(this.orderHeaderResponse)
        },
        error: (error) => {
          console.error('Error getting detail order', error);
        },
      });
    } else {
      console.error('Error getting detail order');
    }
  }

  UpdateOrder() {
    if (this.orderForm.valid) {
      this.orderHeaderData = this.orderForm.value;

      this.orderSer.UpdateOrder(this.orderHeaderData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
          }),
          mergeMap(() =>
            this.orderSer.GetOrder(this.id)
          )
        )
        .subscribe({
          next: (res) => {
            const formattedOrderTime = this.datePipe.transform(res.result.orderTime, 'dd/MM/yyyy');
            this.orderForm.patchValue({
              orderHeaderId: res.result.orderHeaderId,
              name: res.result.name,
              email: res.result.email,
              phone: res.result.phone,
              address: res.result.address,
              orderTime: formattedOrderTime,
              status: res.result.status,
            });
            this.detailOrderDto = res.result.orderDetails;
            this.status = res.result.status!;
            this.orderHeaderResponse=res.result;
          },
        }

        );
    } else { }
  }

  UpdateOrderStatus(newStatus: string) {
    if (this.orderForm.valid) {
      this.orderHeaderData = this.orderForm.value;

      this.orderSer.UpdateOrderStatus(this.id, newStatus)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
          }),
          mergeMap(() =>
            this.orderSer.GetOrder(this.id)
          )
        )
        .subscribe({
          next: (res) => {
            const formattedOrderTime = this.datePipe.transform(res.result.orderTime, 'dd/MM/yyyy');
            this.orderForm.patchValue({
              orderHeaderId: res.result.orderHeaderId,
              name: res.result.name,
              email: res.result.email,
              phone: res.result.phone,
              address: res.result.address,
              orderTime: formattedOrderTime,
              status: res.result.status,
            });
            this.detailOrderDto = res.result.orderDetails;
            this.status = res.result.status!;
            this.orderHeaderResponse=res.result;
          },
        }

        );
    } else {

    }
  }

  CreateStripeSession(){
    if(this.orderHeaderResponse){
      const request: StripeRequestDto = {
        approvedUrl:`http://localhost:4200/orderConfirm/${this.orderHeaderResponse.orderHeaderId}`,
        cancelUrl:`http://localhost:4200/order/${this.orderHeaderResponse.orderHeaderId}`,
        orderHeader:this.orderHeaderResponse

       }
       this.orderSer.CreateStripeSession(request).subscribe({
         next: (res: ResponseDto<StripeRequestDto>) => {
           if(res && res.result.stripeSessionUrl){
             window.location.href = res.result.stripeSessionUrl;
           }
         }, error: (err: any) => {
         }
       })
    }

  }


}

