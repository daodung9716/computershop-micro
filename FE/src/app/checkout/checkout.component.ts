import { Component, OnInit } from '@angular/core';
import { CartDetailsDto, CartDto, CartHeaderDto } from '../models/cart';
import { Router } from '@angular/router';
import { ResponseDto } from '../models/responsedto';
import { CartService } from '../services/cart/cart.service';
import { LoadingService } from '../services/loading.service';
import { OrderService } from '../services/order/order.service';
import { OrderHeaderDto, StripeRequestDto } from '../models/order';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  cartHeader!: CartHeaderDto;
  cartDetail!: CartDetailsDto[];
  orderHeaderResponse!:OrderHeaderDto
  subTotal!: number;
  total!: number;


  constructor(
    private cartService: CartService, private router: Router,
    private loadingService: LoadingService, private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.GetCartCheckout();

  }

  GetCartCheckout() {
    this.cartService.GetCartCheckout().subscribe({
      next: (res: ResponseDto<CartDto>) => {
        if (res && res.result && res.result.cartHeader) {
          this.cartHeader = res.result.cartHeader!;
          this.cartDetail = res.result.cartDetails!;
          this.calculateTotal();
        }
      }, error: (err: any) => {
      }
    })
  }

  CreateOrder(){
    const cart: CartDto = {
     cartHeader:this.cartHeader,
     cartDetails:this.cartDetail

    }
    this.orderService.CreateOrder(cart).subscribe({
      next: (res: ResponseDto<OrderHeaderDto>) => {
       this.orderHeaderResponse=res.result;
       this.CreateStripeSession()
      }, error: (err: any) => {
      }
    })
  }



  CreateStripeSession(){
    const request: StripeRequestDto = {
     approvedUrl:`http://localhost:4200/orderConfirm/${this.orderHeaderResponse.orderHeaderId}`,
     cancelUrl:'http://localhost:4200/checkout',
     orderHeader:this.orderHeaderResponse

    }
    this.orderService.CreateStripeSession(request).subscribe({
      next: (res: ResponseDto<StripeRequestDto>) => {
        if(res && res.result.stripeSessionUrl){
          window.location.href = res.result.stripeSessionUrl;
        }
      }, error: (err: any) => {
      }
    })
  }


  calculateTotal() {
    if (this.cartDetail) {
      this.total = this.cartDetail.reduce(
        (acc, cart) => acc + cart.count! * cart.product!.price!,
        0
      );
    }

  }


}
