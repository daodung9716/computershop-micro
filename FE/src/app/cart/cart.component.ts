import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart/cart.service';
import { Router } from '@angular/router';
import { LoadingService } from '../services/loading.service';
import { CartDetailsDto, CartDto, CartHeaderDto } from '../models/cart';
import { ResponseDto } from '../models/responsedto';
import { ProductVM } from '../models/product';
import { OrderService } from '../services/order/order.service';
import { OrderHeaderDto } from '../models/order';
import { mergeMap, tap } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cartHeader!: CartHeaderDto;
  cartDetail: CartDetailsDto[]=[];
  subTotal!: number;
  total: number =0;


  constructor(
    private cartService: CartService, private router: Router,  private toastr: ToastrService,
    private loadingService: LoadingService, private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.GetCart();

  }

  GetCart() {
    this.cartService.GetCart().subscribe({
      next: (res: ResponseDto<CartDto>) => {
        if (res && res.result && res.result.cartHeader) {
          this.cartHeader = res.result.cartHeader!;
          this.cartDetail = res.result.cartDetails!;
          this.calculateTotal();
        }
      }, error: (err: any) => {
      }
    })
  }



  CartUpsertBulk() {
    const cartDto: CartDto = {
      cartHeader: {},
      cartDetails: this.cartDetail
    }
    this.cartService.CartUpsertBulk(cartDto).subscribe({
      next: (res: ResponseDto<CartDto>) => {
        this.router.navigate(['/checkout']);
      }, error: (err: any) => {
      }
    })
  }

  calculateTotal() {
    if (this.cartDetail) {
      this.total = this.cartDetail.reduce(
        (acc, cart) => acc + cart.count! * cart.product!.price!,
        0
      );
    }

  }

  increaseQuantity(cart: CartDetailsDto) {
    cart.count!++;
    this.calculateTotal();
  }

  decreaseQuantity(cart: CartDetailsDto) {
    if (cart.count! > 1) {
      cart.count!--;
    }

    this.calculateTotal();
  }

  RemoveCartDetail(cardHeaderId:string,cartDetailId :string){
    this.cartService.RemoveCartDetail(cardHeaderId,cartDetailId) .pipe(
      tap((res) => {
        if (res.isSuccess == false) {
          this.toastr.error(` ${res.message}`, `Failed`, {
            timeOut: 5000,
          });
        } else {
          this.toastr.success(` ${res.message}`, `Success`, {
            timeOut: 5000,
          });
        }
      }),
      mergeMap(() =>
      this.cartService.GetCart()
      )
    )
    .subscribe({
      next: (res) => {
        this.cartHeader = res.result.cartHeader!;
        this.cartDetail = res.result.cartDetails!;
        this.calculateTotal();
      },
      error: (error) => {

       },
    })};

  carts = [
    { count: 'x1', name: 'product2', src: '../assets/images/products/product2.jpg', price: '$320' },
    { count: 'x1', name: 'product3', src: '../assets/images/products/product3.jpg', price: '$320' },

    { count: 'x1', name: 'product4', src: '../assets/images/products/product4.jpg', price: '$222' },

    { count: 'x1', name: 'product5', src: '../assets/images/products/product5.jpg', price: '$320' },

    { count: 'x1', name: 'product6', src: '../assets/images/products/product6.jpg', price: '$320' },
  ]
}
