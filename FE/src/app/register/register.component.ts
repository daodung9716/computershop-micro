import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { ResponseDto } from '../models/responsedto';
import { checkEmailValidatorDebounce } from '../helper/checkemailvalidator';
import { RegisterDTO } from '../models/register';
import { tap } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  public role: string = '';
  public isAdminRole: boolean = false;
  isLoggedIn: boolean = false;
  userForm!: FormGroup;
  emailMessageError!: string;
  eyeIcon: string = 'fa-eye-slash';
  isText: boolean = false;
  type: string = 'password';
  defaultEmail!: string;
  idUpdate!: string | null;
  isEmailSame?: boolean;
  userData!: RegisterDTO;

  inputOther = [
    {
      name: 'name',
      placeholder: 'Name',
      type: 'text',
      errors: [
        { key: 'required', message: 'Name is required' },
        { key: 'minlength', message: 'Name must have at least 6 characters' },
      ],
    },
    {
      name: 'address',
      placeholder: 'Address',
      type: 'text',
      errors: [
        { key: 'required', message: 'Address is required' },
        {
          key: 'minlength',
          message: 'Address must have at least 6 characters',
        }
      ],
    },
    {
      name: 'phoneNumber',
      placeholder: 'Phone Number',
      type: 'number',
      errors: [
        { key: 'required', message: 'PhoneNumber is required' },
        {
          key: 'minlength',
          message: 'PhoneNumber must have  10 numbers',
        },
        {
          key: 'maxlength',
          message: 'PhoneNumber must have  10 numbers',
        },
      ],
    },
  ];

  passwordErrors = [
    { name: 'password', key: 'required', message: 'Password is required' },
    {
      name: 'password',
      key: 'minlength',
      message: 'Password must have at least 8 characters',
    },
    {
      name: 'password',
      key: 'maxlength',
      message: 'Password must have at most 20 characters',
    },
    {
      name: 'password',
      key: 'pattern',
      message: 'Password must have at least 1 uppercase, lowercase, and digit',
    },
  ];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,    private toastr: ToastrService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      name: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
      phoneNumber: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(10),
          Validators.maxLength(10),
          Validators.pattern(/^\d{10}$/),
        ]),
      ],
      password: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$'),
        ]),
      ],
      email: [
        '',
        [Validators.required],
        checkEmailValidatorDebounce(this.authService),
      ],
      address: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
      role: ['',Validators.required]
    });
  }



  onCheckEmail() {
    const email = this.userForm.get('email')!.value;

    if (email) {
      this.authService.checkEmailExist(email).subscribe({
        next: (res: ResponseDto<string>) => {
          this.emailMessageError = res.message;
        },
        error: (error) => {
          console.error('Error check email existed', error);
        },
      });
    } else {

    }
  }

  Register() {
    if (this.userForm.valid) {
      this.userData = this.userForm.value;

      this.authService
        .register(this.userData)
        .pipe(
          tap((res) => {
            if (res.isSuccess == false) {
              this.toastr.error(` ${res.message}`, `Failed`, {
                timeOut: 5000,
              });
            } else {
              this.toastr.success(` ${res.message}`, `Success`, {
                timeOut: 5000,
              });
            }
            this.router.navigate(['/login']);
          })
        )
        .subscribe();
    } else {
      this.toastr.error('Invalid Form Data', 'ERROR', {
        timeOut: 5000,
      });
    }
  }

  hideShowPass() {
    this.isText = !this.isText;
    this.isText ? (this.eyeIcon = 'fa-eye') : (this.eyeIcon = 'fa-eye-slash');
    this.isText ? (this.type = 'text') : (this.type = 'password');
  }
}
