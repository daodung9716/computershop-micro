import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CartDto } from 'src/app/models/cart';
import { ResponseDto } from 'src/app/models/responsedto';

@Injectable({
  providedIn: 'root'
})
export class CartService {
 baseUrl='https://localhost:7174/api/cart'
  constructor(private http: HttpClient) { }

  CartUpsert(cartDto: CartDto): Observable<ResponseDto<CartDto>> {
    return this.http.post<ResponseDto<CartDto>>(`${this.baseUrl}/cartUpsert`, cartDto);
  }

  CartUpsertBulk(cartDto: CartDto): Observable<ResponseDto<CartDto>> {
    return this.http.post<ResponseDto<CartDto>>(`${this.baseUrl}/cartUpsertBulk`, cartDto);
  }

  GetCart(): Observable<ResponseDto<CartDto>> {
    return this.http.get<ResponseDto<CartDto>>(`${this.baseUrl}/getCart`);
  }

  GetCartCheckout(): Observable<ResponseDto<CartDto>> {
    return this.http.get<ResponseDto<CartDto>>(`${this.baseUrl}/getCartCheckout`);
  }

  RemoveCart(): Observable<ResponseDto<string>> {
    return this.http.post<ResponseDto<string>>(`${this.baseUrl}/removeCart`,null);
  }

  RemoveCartDetail(cardHeaderId:string,cartDetailId :string): Observable<ResponseDto<string>> {
    return this.http.post<ResponseDto<string>>(`${this.baseUrl}/removeCartDetail?cardHeaderId=${cardHeaderId}&cartDetailId=${cartDetailId}`,null);
  }
}
