import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { Category } from 'src/app/models/category';
import { Product, ProductVM } from 'src/app/models/product';
import { Author } from 'src/app/models/author';
import { ResponseDto } from 'src/app/models/responsedto';
import { Form } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  baseUrl = 'https://localhost:7000/api/product';

  constructor(private http: HttpClient) {}

  GetCategory(page:number,searchField: string |null): Observable<ResponseDto<Category[]>> {
    return this.http.get<ResponseDto<Category[]>>(`${this.baseUrl}/getCategory?page=${page}&searchField=${searchField}`);
  }

  GetAuthor(page:number,searchField: string |null): Observable<ResponseDto<Author[]>> {
    return this.http.get<ResponseDto<Author[]>>(`${this.baseUrl}/getAuthor?page=${page}&searchField=${searchField}`);
  }

  GetAllCategory(): Observable<ResponseDto<Category[]>> {
    return this.http.get<ResponseDto<Category[]>>(`${this.baseUrl}/getAllCategory`);
  }

  GetAllAuthor(): Observable<ResponseDto<Author[]>> {
    return this.http.get<ResponseDto<Author[]>>(`${this.baseUrl}/getAllAuthor`);
  }


  GetRecommend(): Observable<ResponseDto<ProductVM[]>> {
    return this.http.get<ResponseDto<ProductVM[]>>(`${this.baseUrl}/getRecommend`);
  }

  GetBestBuy(): Observable<ResponseDto<ProductVM[]>> {
    return this.http.get<ResponseDto<ProductVM[]>>(`${this.baseUrl}/getBestBuy`);
  }

  GetByIdProduct(id: string): Observable<ResponseDto<ProductVM>> {
    return this.http.get<ResponseDto<ProductVM>>(`${this.baseUrl}/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error fetching get id product', error);
        return throwError(() => error);
      })
    );
  }

  GetRelate(id: string): Observable<ResponseDto<ProductVM[]>> {
    return this.http
      .get<ResponseDto<ProductVM[]>>(`${this.baseUrl}/getRelate/${id}`)
      .pipe(
        tap(),
        catchError((error) => {
          console.error('Error fetching get relate product', error);
          return throwError(() => error);
        })
      );
  }

  GetProductByCategory(id: string): Observable<ResponseDto<ProductVM[]>> {
    return this.http
      .get<ResponseDto<ProductVM[]>>(`${this.baseUrl}/getProductByCategory/${id}`)
      .pipe(
        tap(),
        catchError((error) => {
          console.error('Error fetching get  product by category', error);
          return throwError(() => error);
        })
      );
  }

  GetProductByAuthor(id: string): Observable<ResponseDto<ProductVM[]>> {
    return this.http
      .get<ResponseDto<ProductVM[]>>(`${this.baseUrl}/getProductByAuthor/${id}`)
      .pipe(
        tap(),
        catchError((error) => {
          console.error('Error fetching get  product by author', error);
          return throwError(() => error);
        })
      );
  }

  GetProductByAuthorCategory(
    idAuthor: string | null,idCategory: string | null, minPrice: number | null, maxPrice: number | null
  ): Observable<ResponseDto<ProductVM[]>> {
    let queryParams = '';

    if (idAuthor !== null) {
      queryParams += `idauthor=${idAuthor}`;
    }

    if (idCategory !== null) {
      if (queryParams.length > 0) {
        queryParams += '&';
      }
      queryParams += `idcategory=${idCategory}`;
    }

    if (minPrice !== null) {
      if (queryParams.length > 0) {
        queryParams += '&';
      }
      queryParams += `minPrice=${minPrice}`;
    }

    if (maxPrice !== null) {
      if (queryParams.length > 0) {
        queryParams += '&';
      }
      queryParams += `maxPrice=${maxPrice}`;
    }

    const url = `${this.baseUrl}/getProductByAuthorCategory?${queryParams}`;

    return this.http.get<ResponseDto<ProductVM[]>>(url).pipe(
      tap(),
      catchError((error) => {
        console.error('Error fetching get product by author', error);
        return throwError(() => error);
      })
    );
  }


  GetProduct(page:number,searchField: string |null): Observable<ResponseDto<ProductVM[]>>{
    return this.http
    .get<ResponseDto<ProductVM[]>>(`${this.baseUrl}?page=${page}&searchField=${searchField}`)
    .pipe(
      tap(),
      catchError((error) => {
        console.error('Error fetching get  product ', error);
        return throwError(() => error);
      })
    );

  }

  UpsertCategory(formData:FormData): Observable<ResponseDto<any>>{
    return this.http
    .post<ResponseDto<any>>(`${this.baseUrl}/upsertCategory`,formData)
    .pipe(
      tap(),
      catchError((error) => {
        console.error('Error upsert Category', error);
        return throwError(() => error);
      })
    );
  }

  UpsertProduct(formData:FormData): Observable<ResponseDto<any>>{

    return this.http
    .post<ResponseDto<any>>(`${this.baseUrl}/upsertProduct`,formData)
    .pipe(
      tap(),
      catchError((error) => {
        console.error('Error upsert Product', error);
        return throwError(() => error);
      })
    );
  }


  UpsertAuthor(author:Author): Observable<ResponseDto<any>>{
    return this.http
    .post<ResponseDto<any>>(`${this.baseUrl}/upsertAuthor`,author)
    .pipe(tap(),
      catchError((error) => {
        console.error('Error upsert Author', error);
        return throwError(() => error);
      })
    );
  }

  DeleteCategory(id: string): Observable<ResponseDto<any>> {
    return this.http.delete<ResponseDto<any>>(`${this.baseUrl}/deleteCategory?id=${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error delete category', error);
        return throwError(() => error);
      })
    );
  }

  DeleteAuthor(id: string): Observable<ResponseDto<any>> {
    return this.http.delete<ResponseDto<any>>(`${this.baseUrl}/deleteAuthor?id=${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error delete author', error);
        return throwError(() => error);
      })
    );
  }

  DeleteProductImage(id: string): Observable<ResponseDto<any>> {
    return this.http.delete<ResponseDto<any>>(`${this.baseUrl}/deleteProductImage/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error delete ProductImage', error);
        return throwError(() => error);
      })
    );
  }

  DeleteProduct(id: string): Observable<ResponseDto<any>> {
    return this.http.delete<ResponseDto<any>>(`${this.baseUrl}/deleteProduct/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error delete Product', error);
        return throwError(() => error);
      })
    );
  }



  DetailCategory(id: string): Observable<ResponseDto<Category>> {
    return this.http.get<ResponseDto<Category>>(`${this.baseUrl}/getByIdCategory/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error get detail category', error);
        return throwError(() => error);
      })
    );
  }

  DetailAuthor(id: string): Observable<ResponseDto<Author>> {
    return this.http.get<ResponseDto<Author>>(`${this.baseUrl}/getByIdAuthor/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error get detail author', error);
        return throwError(() => error);
      })
    );
  }


}
