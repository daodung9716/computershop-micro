import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, catchError, tap, throwError } from 'rxjs';
import { StoreTokenService } from './storetoken.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginModel, LoginResponse, TokenApiModel, UserDto } from 'src/app/models/login';
import { ResponseDto } from 'src/app/models/responsedto';
import { RegisterDTO } from 'src/app/models/register';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = 'https://localhost:7002/api/auth';

  private userPayLoad: any;

   isLoggedIn: BehaviorSubject<boolean> ;

   constructor(private http: HttpClient, private router: Router,private storeTokenService:StoreTokenService) {
    this.userPayLoad = this.decodeToken();
    //console.log( this.userPayLoad);
    const isLoggedIn = localStorage.getItem('isLoggedIn') === 'true';
    //isLoggedIn sẽ bằng true nếu  localStorage.setItem('isLoggedIn', 'true');
    this.isLoggedIn = new BehaviorSubject<boolean>(isLoggedIn);
  }



  //lưu trữ TOKEN
  storeToken(tokenValue: string) {
    localStorage.setItem('token', tokenValue);
  }

  storeRefreshToken(tokenValue: string) {
    localStorage.setItem('refreshToken', tokenValue);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getRefreshToken() {
    return localStorage.getItem('refreshToken');
  }

  decodeToken() {
    const jwtHelper = new JwtHelperService();
    const token = this.getToken()!;
    return jwtHelper.decodeToken(token);
  }

  getFullNameFromToken() {
    if (this.userPayLoad) {
      return this.userPayLoad.name;
    }
  }
  getRoleFromToken() {
    if (this.userPayLoad) {
      return this.userPayLoad.role;
    }
  }

  renewToken(tokenApi: TokenApiModel): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.baseUrl}/refresh`, tokenApi);
  }



  storeFullName(fullName: string) {
    localStorage.setItem('fullName', fullName);
  }


  Login(model: LoginModel): Observable<ResponseDto<LoginResponse>> {
    return this.http.post<ResponseDto<LoginResponse>>(`${this.baseUrl}/login`, model).pipe(
      tap(() => {
       this.LogInTrue();
      })
    );
  }

  Logout() {
    localStorage.clear();
    this.isLoggedIn.next(false);
    this.storeTokenService.updateFullName('');
    this.storeTokenService.updateRoleName('');
    this.router.navigate(['/login']);

  }

  LogInTrue() {
    this.isLoggedIn.next(true);
    localStorage.setItem('isLoggedIn', 'true');
  }

  checkEmailExist(email: string): Observable<ResponseDto<string>> {
    return this.http
      .get<ResponseDto<string>>(`${this.baseUrl}/checkEmailExist/${email}`)
      .pipe(
        tap(),
      );
  }

  register(user: UserDto): Observable<ResponseDto<string>> {
    return this.http.post<ResponseDto<string>>(`${this.baseUrl}/register`, user).pipe(
      tap(),
      catchError((error) => {
        console.error('Error register user', error);
        return throwError(() => error);
      })
    );
  }

  GetUsers(page:number, searchQuery:string|null): Observable<ResponseDto<UserDto[]>> {
    return this.http.get<ResponseDto<UserDto[]>>(`${this.baseUrl}/getUsers?page=${page}&searchField=${searchQuery}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error get user', error);
        return throwError(() => error);
      })
    );
  }

  GetByIdUser(id:string): Observable<ResponseDto<UserDto>> {
    return this.http.get<ResponseDto<UserDto>>(`${this.baseUrl}/getByIdUser/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error get by id user', error);
        return throwError(() => error);
      })
    );
  }

  UpdateUser(dto:UserDto): Observable<ResponseDto<string>> {
    return this.http.post<ResponseDto<string>>(`${this.baseUrl}/updateUser`,dto).pipe(
      tap(),
      catchError((error) => {
        console.error('Error update user', error);
        return throwError(() => error);
      })
    );
  }

  DeleteUser(id:string): Observable<ResponseDto<string>> {
    return this.http.delete<ResponseDto<string>>(`${this.baseUrl}/deleteUser/${id}`).pipe(
      tap(),
      catchError((error) => {
        console.error('Error delete user', error);
        return throwError(() => error);
      })
    );
  }



}
