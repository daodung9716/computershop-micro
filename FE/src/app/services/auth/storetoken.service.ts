import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreTokenService {

  private baseUrl: string = 'https://localhost:7002/api/auth';


  // lưu trữ thông tin tên đầy đủ và vai trò của người dùng.
  private fullName$ = new BehaviorSubject<string>("");
  private role$ = new BehaviorSubject<string>("");


  constructor(private http: HttpClient,private router:Router) { }

  //được sử dụng để trả về role$ là một Observable,để component khác có thể subscribe và theo dõi thay đổi của giá trị role.
  public getRoleFromStore(){
    return this.role$.asObservable();
  }

// được sử dụng để cập nhật giá trị mới cho biến role$.Sau khi được cập nhật, giá trị mới được phát đi cho tất cả các component đã subscribe vào role$.
  public setRoleForStore(role:string){
    this.role$.next(role);
  }

public updateFullName(value: string) {
  this.fullName$.next(value);
}
public updateRoleName(value: string) {
  this.role$.next(value);
}
  public getFullNameFromStore(){
  return  this.fullName$.asObservable();
  }

  public setFullNameForStore(fullname:string){
    this.fullName$.next(fullname);
  }


}
