import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor() { }

  isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  startLoading() {
    this.isLoading.next(true);
  
  }

  stopLoading() {
    this.isLoading.next(false);

  }
}
