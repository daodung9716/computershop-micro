import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StoreTokenService } from './auth/storetoken.service';


@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient, private router: Router,private storeToken:StoreTokenService) {}


storeRefreshTokenExpire(refreshTokenExpire: string) {
  localStorage.setItem('refreshTokenExpire', refreshTokenExpire);
}


checkRefreshTokenExpire(): boolean {
  const refreshTokenExpire = localStorage.getItem('refreshTokenExpire');
  if (!refreshTokenExpire) {
    // Nếu không tồn tại refreshTokenExpire trong localStorage, coi như đã hết hạn
    return true;
  }
  const expireDate = new Date(refreshTokenExpire);
  const currentDate = new Date();
  return currentDate > expireDate;
}

}
