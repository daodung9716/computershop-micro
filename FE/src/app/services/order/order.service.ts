import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CartDto } from 'src/app/models/cart';
import { OrderHeaderDto, OrderHeaderUpdate, StripeRequestDto } from 'src/app/models/order';
import { ResponseDto } from 'src/app/models/responsedto';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  baseUrl='https://localhost:7155/api/order'
  constructor(private http: HttpClient) { }


  GetAllOrders(): Observable<ResponseDto<OrderHeaderDto[]>> {
    return this.http.get<ResponseDto<OrderHeaderDto[]>>(`${this.baseUrl}/getAllOrders`);
  }

  GetOrder(id:string): Observable<ResponseDto<OrderHeaderDto>> {
    return this.http.get<ResponseDto<OrderHeaderDto>>(`${this.baseUrl}/getOrder/${id}`);
  }


  CreateOrder(cartDto: CartDto): Observable<ResponseDto<OrderHeaderDto>> {
    return this.http.post<ResponseDto<OrderHeaderDto>>(`${this.baseUrl}/createOrder`, cartDto);
  }

  UpdateOrder(dto: OrderHeaderUpdate): Observable<ResponseDto<string>> {
    return this.http.post<ResponseDto<string>>(`${this.baseUrl}/updateOrder`, dto);
  }


  CreateStripeSession(request: StripeRequestDto): Observable<ResponseDto<StripeRequestDto>> {
    return this.http.post<ResponseDto<StripeRequestDto>>(`${this.baseUrl}/createStripeSession`, request);
  }

  ValidateStripeSession(orderHeaderId: number): Observable<ResponseDto<OrderHeaderDto>> {
    return this.http.post<ResponseDto<OrderHeaderDto>>(`${this.baseUrl}/validateStripeSession`, orderHeaderId);
  }

  UpdateOrderStatus(orderId:string, newStatus:string): Observable<ResponseDto<string>> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<ResponseDto<string>>(`${this.baseUrl}/updateOrderStatus/${orderId}`, JSON.stringify(newStatus), { headers: headers });
  }



}


