import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { OrderComponent } from './order/order.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AccountComponent } from './account/account.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ShopComponent } from './shop/shop.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { ToastrModule } from 'ngx-toastr';
import { ProcessingIconComponent } from './processing-icon/processing-icon.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderConfirmComponent } from './order-confirm/order-confirm.component';
import { ProductComponent } from './product/product.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { CategoryComponent } from './admin/category/category.component';
import { OrderDetailComponent } from './order/order-detail/order-detail.component';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    HomeComponent,
    OrderComponent,
    LoginComponent,
    RegisterComponent,
    AccountComponent,
    ShopComponent,
    ProcessingIconComponent,
    CartComponent,
    CheckoutComponent,
    OrderConfirmComponent,
    OrderDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule ,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
    })
  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
       multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
