import { ProductDto } from "./product";

export class OrderHeaderDto{
  orderHeaderId?:string;
  userId?:string;
  couponCode?:string;
  discount?:number;
  orderTotal?:number;
  name?:string;
  phone?:string;
  email?:string;
  address?:string;
  orderTime?:Date;
  status?:string;
  paymentIntentId?:string;
  stripeSessionId?:string;
  orderDetails:OrderDetailsDto[]=[];
}

export class OrderHeaderUpdate{
  orderHeaderId?:string;
  name?:string;
  phone?:string;
  email?:string;
  address?:string;
}

export class OrderDetailsDto{
  orderDetailsId?:string;
  orderHeaderId?:string;
  productId?:string;
  product?:ProductDto;
  count!:number;
  productName?:string;
  price?:number;
}

export class StripeRequestDto{
  stripeSessionUrl?:string;
  stripeSessionId?:string;
  approvedUrl?:string;
  cancelUrl?:string;
  orderHeader?: OrderHeaderDto

}
