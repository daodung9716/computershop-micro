import { ProductDto } from "./product";

export class CartDto{
  cartHeader?: CartHeaderDto;
  cartDetails?: CartDetailsDto[]
}

export class CartHeaderDto{
  cartHeaderId?:string;
  userId?:string;
  couponCode?:string;
  discount?:string;
  cartTotal?:string;
  name?:string;
  phone?:string;
  email?:string;
  address?:string
}

export class CartDetailsDto{
  cartDetailsId?:string;
  cartHeaderId?:string;
  cartHeader?:CartHeaderDto;
  productId?:string;
  product?:ProductDto;
  count!:number;
}
