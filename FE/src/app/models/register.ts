export interface RegisterDTO{
  email:string;
  name:string;
  address:string;
  phoneNumber:string;
  password:string;
  role:string;
}
