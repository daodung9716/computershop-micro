export interface LoginResponse{
  User:UserDto
  token: string;
  refreshToken:string;
}

export class TokenApiModel{
  accessToken!:string;
  refreshToken!:string;
}

export interface ForgetModel {
  email: string;
}

export interface LoginModel {
  userName: string;
  password: string;
}

export class UserDto{
  id?:string;
  email?:string;
  name?:string;
  phoneNumber?:string;
  address?:string;
  role?:string;
  password?:string;
}
