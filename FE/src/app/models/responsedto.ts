export interface ResponseDto<T> {
  result: T ;
  isSuccess: boolean;
  message: string ;
  pageSize:number;
  totalPages:number;
  recordsTotal:number;
  recordPerPage:number;
}
