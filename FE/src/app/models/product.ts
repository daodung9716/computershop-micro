import { Author } from "./author";
import { Category } from "./category";


export interface Product {
  id: string;
  name: string;
  price: number ;
  listPrice:number;
  description:string;
  imageUrl:string;
  buyCount:number;
  author: Author;
  category: Category;
  productImages: ProductImage[];

}

export interface ProductImage {
  id: string;
  imageUrl: string;

}


export interface ProductVM {
  id: string;
  name: string;
  price: number ;
  listPrice:number;
  description:string;
  buyCount:number;
  categoryName?:string;
  authorName?:string;
  categoryId?:number;
  authorId?:number;
  imageUrl:string;
  productImages: ProductImage[];

}

export class ProductDto{
  id?:string;
  name?:string;
  price?:number;
  description?:string;
  categoryId?:number;
  authorId?:number;
  imageUrl?:string;
  productImages: ProductImage[]=[];
}

