/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./src/**/*.{html,js,ts}',
   './src/index.html',
   './app/**/*.{html,js,ts}',
   './src/app/**/*.{html,js,ts}'
   ],
  purge: [
    './src/**/*.html',
    './src/**/*.js',
    './src/**/*.ts',
  ],
  theme: {
    container: {
      center: true,
    },
    screens:{
      xss:'260px',
      xs:'350px',
      sm: '480px',
      xsm: '600px',
      md: '768px',
      lg: '976px',
      l:'1200px',
      xl: '1440px',
    },
    extend: {
      colors: {
          'tail-white':'#ffffff',
          'tail-100':'#48d8ed',
          'tail-300':'#1fc3dd',
          'tail-black':'#111827',
          'tail-pink':'#ef4899',
          'tail-gray':'#6b7f98',
      },
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      borderRadius: {
        '4xl': '2rem',
      },
      width:{
        '82':'21rem',//336px
        '85':'22rem',//352px
        '90':'24rem',//384px
        '100':'26rem',
        '102':'27rem',
        '104':'28rem',
        '110':'30rem',
        '130':'36rem',//576px

      }
    },

    fontSize: {
      's':'9px',
      'ssm':'11px',
      'sm': '12px',
      'base0':'14px',
      'base': '16px',
      'base1':'18px',
      'xl': '20px',
      '1xl': '23px',
      '2xl': '25px',
      '3xl': '31px',
      '4xl': '39px',
      '5xl': '60px',
      'txl':'72px'
    }
  },
  plugins: [],
}
